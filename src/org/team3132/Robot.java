/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package org.team3132;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.jibble.simplewebserver.SimpleWebServer;
import org.team3132.Behaviors.*;
import org.team3132.lib.Test;
import org.team3132.lib.logging.Log;
import org.team3132.lib.logging.LogDygraph;
import org.team3132.lib.util.EventJoystick;
import org.team3132.lib.util.RingCounter;
import org.team3132.physicalTest.TestSelector;
import org.team3132.subsystems.Drivebase;

import java.io.File;

import static org.team3132.Behaviors.BasicStance.*;


public class Robot extends IterativeRobot {

    enum RobotMode {
        robotInit,
        disabled,
        auto,
        teleop,
        test
    }

    RobotMode robotMode = RobotMode.robotInit;
    double robotPreviousTime = 0;
    double robotElapsedTime = 0;
    double teleopPreviousTime = 0;
    double teleopElapsedTime = 0;

    EventJoystick driver = new EventJoystick(0);
    DriveHelper dh = new DriveHelper();
    AutomatedRunner ar = new AutomatedRunner();

    TestSelector testSelector = new TestSelector();
    Test test = null;

    Drivebase drivebase = Drivebase.getInstance();
    boolean hasClimbPrepped = false;


    // logging
    Thread loggingThread;
    public static Log log = new LogDygraph(Constants.LOG_BASE_PATH, Constants.LOG_DATA_EXTENSION, Constants.LOG_DATE_EXTENSION, Constants.LOG_NUMBER_FILE, false);
    File fileDir;


	@Override
	public void robotInit() {
        robotMode = RobotMode.robotInit;

        LiveWindow.setEnabled(false);

        SmartDashboard.putBoolean("Propulsion Velocity Mode", Constants.PROPULSION_VELOCITY_MODE);

        fileDir = new File(Constants.WEB_BASE_PATH);

        loggingThread = new Thread(log);
        loggingThread.start();

        try {
            new SimpleWebServer(fileDir, Constants.WEB_PORT);
            log.sub("WebServer started at port: " + Constants.WEB_PORT);
        } catch (Exception e) {
            log.sub("Failed to start webserver on directory " + fileDir.getAbsolutePath());

            e.printStackTrace();
        }

        DriverStation driverStation = DriverStation.getInstance();


        log.register(false, Timer::getMatchTime, "DriverStation/MatchTime")
                .register(true, () -> teleopElapsedTime, "DriverStation/teleopPeriodicPeriod")
                .register(true, () -> robotElapsedTime, "DriverStation/robotPeriodicPeriod")
                .register(true, RobotController::getBatteryVoltage, "RobotController/BatteryVoltage")
                .register(true, () -> RobotController.isBrownedOut()?1:0, "RobotController/isBrownedOut")
                .register(true, RobotController::getCurrent3V3,"RobotController/current3V3")
                .register(true, RobotController::getCurrent5V, "RobotController/current5V")
                .register(true, RobotController::getCurrent6V, "RobotController/current6V")
                .register(true, RobotController::getVoltage3V3, "RobotController/Voltage3V3")
                .register(true, RobotController::getVoltage5V, "RobotController/Voltage5V")
                .register(true, RobotController::getVoltage6V, "RobotController/Voltage6V");




        String matchDescription = String.format("%s_%s_M%d_R%d_%s_P%d", driverStation.getEventName(),
                driverStation.getMatchType().toString(), driverStation.getMatchNumber(), driverStation.getReplayNumber(),
                driverStation.getAlliance().toString(), driverStation.getLocation());
        log.logCompletedElements(matchDescription);

    }
	
	@Override
	public void robotPeriodic() {
        robotElapsedTime = Timer.getFPGATimestamp() - robotPreviousTime;
        robotPreviousTime = Timer.getFPGATimestamp();

	    SmartDashboard.putNumber("Robot Periodic Cycle Time", robotElapsedTime);
		SmartDashboard.putString("Robot Mode", robotMode.toString());

		drivebase.updateSmartDashboard();

	}
	
	@Override
	public void autonomousInit() {
		robotMode = RobotMode.auto;
	}
	
	@Override
	public void autonomousPeriodic() {
		
	}
	
	@Override
	public void teleopInit() {
		robotMode = RobotMode.teleop;

		drivebase.cleanup();

	}
	
	@Override
	public void teleopPeriodic() {
        teleopElapsedTime = Timer.getFPGATimestamp() - teleopPreviousTime;
        teleopPreviousTime = Timer.getFPGATimestamp();
        SmartDashboard.putNumber("Teleop Periodic Cycle Time", teleopElapsedTime);

        dh.smartDrive(driver);


        double moveSpeed = 3;

        if(driver.wasPressed(9)) {
            //BasicStance.standUp();
            if(ar.isRunning()) {
                ar.kill();
            } else if(!hasClimbPrepped){
                stairPrep();
                hasClimbPrepped = true;
            } else {
                if(!ar.isRunning()) {
                    ar.setRoutine(new ClimbStairSingleLeg());
                    ar.start();
                }
            }
        } else if(driver.isHeld(10)) {
            hasClimbPrepped = false;
            if(drivebase.isInMecanumMode()) {
                if(!ar.isRunning()) {
                    ar.setRoutine(new WaggleArms());
                    ar.start();
                }
            } else {
                handsUp();
            }

        } else if(driver.isHeld(4)) {
            tipForward();
           // drivebase.moveFrontPivotsIncrement(-moveSpeed);
        } else if(driver.isHeld(2)) {
            tipBack();
            //drivebase.moveFrontPivotsIncrement(moveSpeed);
        } else if(driver.isHeld(1)) {
            tipLeft();
        } else if(driver.isHeld(3)) {
            tipRight();
        }


        if(driver.isHeld(5)) {
            //drivebase.moveBackPivotsIncrement(-moveSpeed);
            //drivebase.moveBackLeftPivotIncrement(-moveSpeed);
            BasicStance.standUp();
        } else if(driver.wasPressed(7)) {
            //drivebase.moveBackPivotsIncrement(moveSpeed);
            //drivebase.moveBackLeftPivotIncrement(moveSpeed);
            if(drivebase.isInMecanumMode()) {
                BasicStance.surprised();
            } else {
                if (!ar.isRunning()) {
                    ar.setRoutine(new AroundTheWorld(AroundTheWorld.Direction.antiClockwise));
                    ar.start();
                } else {
                    ar.kill();
                }
            }
        }

        if(driver.isHeld(6)) {
            //drivebase.moveBackRightPivotIncrement(-moveSpeed);
            if(!ar.isRunning()) {
                ar.setRoutine(new WiggleButt());
                ar.start();
            }
        } else if(driver.wasPressed(8)) {
            //drivebase.moveBackRightPivotIncrement(moveSpeed);
            if(drivebase.isInMecanumMode()) {
                BasicStance.pointFrontRight();
            } else {
                if (!ar.isRunning()) {
                    ar.setRoutine(new AroundTheWorld(AroundTheWorld.Direction.clockwise));
                    ar.start();
                } else {
                    ar.kill();
                }
            }
        }


	}
	
	@Override
	public void testInit() {
	    LiveWindow.setEnabled(false);
	    robotMode = RobotMode.test;

        test = testSelector.getSelected();
        if(test != null) {
            test.testInit();
        }
	}
	
	@Override
	public void testPeriodic() {
	    if(test != null) {
	        test.testPeriodic();
        }
	}

	@Override
	public void disabledInit() {
        if(robotMode == RobotMode.test) {
            test.testEnd();
            test = null;
        } else if(robotMode == RobotMode.teleop) {
            teleopElapsedTime = 0;
            ar.kill();
        }
        hasClimbPrepped = false;
        drivebase.cleanup();
        robotMode = RobotMode.disabled;
    }

    @Override
	public void disabledPeriodic() {
        testSelector.update();

        Constants.PROPULSION_VELOCITY_MODE = SmartDashboard.getBoolean("Propulsion Velocity Mode", Constants.PROPULSION_VELOCITY_MODE);
    }



}
