package org.team3132.lib.util;


import edu.wpi.first.wpilibj.Joystick;

/**
 * Wrapped Joystick with an edge and level triggered button queries.
 * This allows single action wasPressed() and wasReleased() queries,
 * and isHeld() for single or multi-cycle senses.
 * 
 * The goal is to ensure that the code is only run ONCE on each edge case, but multiple times for the level cases.
 */

public class EventJoystick extends Joystick {
	public static final int NUM_JOYSTICK_BUTTONS = 32;	// maximum number of push buttons on a joystick
	public static final int NUM_JOYSTICK_DIRECTIONS = 10;

	
	private boolean[] buttonStatePressed = new boolean[NUM_JOYSTICK_BUTTONS];
	private boolean[] buttonStateReleased = new boolean[NUM_JOYSTICK_BUTTONS];
	private boolean[] axisStatePressed = new boolean[NUM_JOYSTICK_DIRECTIONS];
	private boolean[] axisStateReleased = new boolean[NUM_JOYSTICK_DIRECTIONS];
	
    public EventJoystick(final int port) {
        super(port);
        for (int i = 0; i < NUM_JOYSTICK_BUTTONS; i++) {
        	buttonStatePressed[i] = false;
        	buttonStateReleased[i] = false;
        }
    }
    
    /**
     * Return true is the button was just pressed. This only returns true once for each press.
     * @param buttonNumber
     * @return true if the button has just changed from released to pressed, false otherwise
     */
    public boolean wasPressed(int buttonNumber) {
    	if (buttonNumber >= NUM_JOYSTICK_BUTTONS) return false;		// not a valid button
    	boolean oldState = buttonStatePressed[buttonNumber];
    	boolean newState = this.getRawButton(buttonNumber);
    	buttonStatePressed[buttonNumber] = newState;
    	return (oldState == false) && (newState == true);
    }
    
    /**
     * 
     * @param axisNumber the axis you want to read
     * @param direction the direction (given in 1 or 0)
     * @return true if the axis has just changed from not pressed to pressed
     */
    public boolean axisWasPressed(int axisNumber, int direction) {
    	boolean oldState = axisStatePressed[axisNumber + direction];
    	boolean newState = false;
    	if (direction == 1) {
    		newState = this.getRawAxis(axisNumber) > 0.75;
    	} else {
    		newState = this.getRawAxis(axisNumber) < -0.75;
    	}
    	axisStatePressed[axisNumber + direction] = newState;
    	return (oldState == false) && (newState == true);
    }
    
    /**
     * 
     * @param axisNumber the axis you want to read
     * @param direction the direction (given in 1 or 0)
     * @return true if the axis has just changed from pressed to not pressed
     */
    public boolean axisWasReleased(int axisNumber, int direction) {
    	boolean oldState = axisStateReleased[axisNumber + direction];
    	boolean newState = false;
    	if (direction == 1) {
    		newState = this.getRawAxis(axisNumber) > 0.75;
    	} else {
    		newState = this.getRawAxis(axisNumber) < -0.75;
    	}
    	axisStateReleased[axisNumber + direction] = newState;
    	return (oldState == true) && (newState == false);
    }

    /**
     * Return true is the button was just released. This only returns true once for each press.
     * @param buttonNumber
     * @return true if the button has just changed from pressed to released, false otherwise
     */
    public boolean wasReleased(int buttonNumber) {
    	if (buttonNumber >= NUM_JOYSTICK_BUTTONS) return false;		// not a valid button
    	boolean oldState = buttonStateReleased[buttonNumber];
    	boolean newState = this.getRawButton(buttonNumber);
    	buttonStateReleased[buttonNumber] = newState;
    	return (oldState == true) && (newState == false);
    }

    /**
     * Return true if the button is held down. A single sense is also considered held to
     * enable quicker response. While the button is pressed this will return true.
     * @param buttonNumber
     * @return true if the button is pressed, false otherwise
     */
    public boolean isHeld(int buttonNumber) {
    	if (buttonNumber >= NUM_JOYSTICK_BUTTONS) return false;		// not a valid button
    	return (this.getRawButton(buttonNumber));
    }
    
    /**
     * Scale a value into a dead zoned squared value. This tends to emphasise the outer range but reduce the
     * inner range. 
     * @param value The value to scale
     * @return The resultant scaled value. There is also a dead zone from -0.1 to 0.1
     */
    private static double scaleAxis(double value) {
    	double deadzone = 0.005;		// approx equal to (1 - (root(1.01)))
    	double deadzoneSqd = (1 + deadzone) * (1 + deadzone);
    	double out = (value * value * deadzoneSqd) - (deadzoneSqd - 1);
		if(out > 0) {
			return out * Math.signum(value);
		} else {
			return 0.0;
		}
    }
    
    /**
     * Return the joystick axis scaled and with a dead zone.
     * @param axis The joystick axis to return
     * @return The scaled value of the axis
     */
    public double getScaledAxis(int axis) {
    	return scaleAxis(this.getRawAxis(axis));
    }
    
    
    /**
     * Return the axis as a boolean value (useful for gamepad triggers)
     * @param axis The axis to return
     * @return The boolean value of the axis
     */
    public boolean isAxisAsButtonHeld(int axis) {
    	return Math.abs(getScaledAxis(axis)) > 0.75;
    }
    
}
