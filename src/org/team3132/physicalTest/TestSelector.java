package org.team3132.physicalTest;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.team3132.lib.Test;
import org.team3132.lib.util.EmptyTest;
import org.team3132.lib.util.FastSendableChooser;

public class TestSelector {

    FastSendableChooser<Test> testChooser = new FastSendableChooser<>();



    public TestSelector() {
        SmartDashboard.putData(testChooser);

        testChooser.addDefault("empty", new EmptyTest());
        testChooser.addObject("Single Pivot", new SinglePivotTest());
        testChooser.addObject("Full Pivot", new FullPivotTest());
        testChooser.addObject("Demo Propulsion", new PropulsionDemo());
        testChooser.addObject("Full Propulsion", new FullPropulsionTest());
        testChooser.addObject("Sensor Data", new SensorTest());

    }

    public void update() {
        testChooser.update();
    }

    public Test getSelected() {
        return testChooser.getSelected();
    }
}
