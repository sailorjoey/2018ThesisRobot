package org.team3132.lib.util;

import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.hal.HAL;
import edu.wpi.first.wpilibj.hal.FRCNetComm.tInstances;
import edu.wpi.first.wpilibj.hal.FRCNetComm.tResourceType;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

/**
 * This class allows us to modify the fundamental functionality and call priority of the base robot code
 * 
 * Key Changes:
 *  - moved robotperiodic to be first in the loop
 *  - removed livewindow
 * 
 * @author Joey
 *
 */
public class ThunderRobot extends RobotBase {

	private boolean m_disabledInitialized;
	private boolean m_autonomousInitialized;
	private boolean m_teleopInitialized;
	private boolean m_testInitialized;

	// robot mode tracking
	public enum RobotMode {
		booting,
		auto,
		teleop,
		test,
		disabled
	}
	protected RobotMode mode = RobotMode.booting;
	protected RobotMode previousMode = null;

	/**
	 * Constructor for RobotIterativeBase.
	 *
	 * <p>The constructor initializes the instance variables for the robot to indicate the status of
	 * initialization for disabled, autonomous, and teleop code.
	 */
	public ThunderRobot() {
		// set status for initialization of disabled, autonomous, and teleop code.
		m_disabledInitialized = false;
		m_autonomousInitialized = false;
		m_teleopInitialized = false;
		m_testInitialized = false;
	}

	/**
	 * Provide an alternate "main loop" via startCompetition().
	 */
	public void startCompetition() {
		HAL.report(tResourceType.kResourceType_Framework,
				tInstances.kFramework_Iterative);
		mode = RobotMode.booting;
		robotInit();

		// Tell the DS that the robot is ready to be enabled
		HAL.observeUserProgramStarting();
		LiveWindow.setEnabled(false);

		// loop forever, calling the appropriate mode-dependent function
		while (true) {
			// Wait for new data to arrive
			m_ds.waitForData();

			try {
				robotPeriodic();
			} catch (Exception e) {
				//System.out.println(e.getMessage());
			}
			
			if(mode != previousMode) {
				previousMode = mode;
				modeChanged();
			}
			
			// Call the appropriate function depending upon the current robot mode
			if (isDisabled()) {
				// call DisabledInit() if we are now just entering disabled mode from
				// either a different mode or from power-on
				mode = RobotMode.disabled;
				if (!m_disabledInitialized) {
					disabledInit();
					m_disabledInitialized = true;
					// reset the initialization flags for the other modes
					m_autonomousInitialized = false;
					m_teleopInitialized = false;
					m_testInitialized = false;
				}
				HAL.observeUserProgramDisabled();
				disabledPeriodic();

			} else if (isTest()) {
				// call TestInit() if we are now just entering physicalTest mode from either
				// a different mode or from power-on
				mode = RobotMode.test;
				if (!m_testInitialized) {
					testInit();
					m_testInitialized = true;
					m_autonomousInitialized = false;
					m_teleopInitialized = false;
					m_disabledInitialized = false;
				}
				HAL.observeUserProgramTest();
				testPeriodic();
				
			} else if (isAutonomous()) {
				// call Autonomous_Init() if this is the first time
				// we've entered autonomous_mode
				mode = RobotMode.auto;
				if (!m_autonomousInitialized) {
					// KBS NOTE: old code reset all PWMs and relays to "safe values"
					// whenever entering autonomous mode, before calling
					// "Autonomous_Init()"
					autonomousInit();
					m_autonomousInitialized = true;
					m_testInitialized = false;
					m_teleopInitialized = false;
					m_disabledInitialized = false;
				}
				HAL.observeUserProgramAutonomous();
				autonomousPeriodic();
				
			} else {
				// call Teleop_Init() if this is the first time
				// we've entered teleop_mode
				mode = RobotMode.teleop;
				if (!m_teleopInitialized) {
					teleopInit();
					m_teleopInitialized = true;
					m_testInitialized = false;
					m_autonomousInitialized = false;
					m_disabledInitialized = false;
				}
				HAL.observeUserProgramTeleop();
				teleopPeriodic();
			}

			
		}
	}
	
	/**
	 * Code called whenever the operation mode of the robot changes. it is one of the first things called in that time.
	 */
	public void modeChanged() {
		System.out.println("Default ThunderRobot.modeChanged() method... Overload me!");
	}

	/* ----------- Overridable initialization code ----------------- */

	/**
	 * Robot-wide initialization code should go here.
	 *
	 * <p>Users should override this method for default Robot-wide initialization which will be called
	 * when the robot is first powered on. It will be called exactly one time.
	 *
	 * <p>Warning: the Driver Station "Robot Code" light and FMS "Robot Ready" indicators will be off
	 * until RobotInit() exits. Code in RobotInit() that waits for enable will cause the robot to
	 * never indicate that the code is ready, causing the robot to be bypassed in a match.
	 */
	public void robotInit() {
		System.out.println("Default IterativeRobot.robotInit() method... Overload me!");
	}

	/**
	 * Initialization code for disabled mode should go here.
	 *
	 * <p>Users should override this method for initialization code which will be called each time the
	 * robot enters disabled mode.
	 */
	public void disabledInit() {
		System.out.println("Default IterativeRobot.disabledInit() method... Overload me!");
	}

	/**
	 * Initialization code for autonomous mode should go here.
	 *
	 * <p>Users should override this method for initialization code which will be called each time the
	 * robot enters autonomous mode.
	 */
	public void autonomousInit() {
		System.out.println("Default IterativeRobot.autonomousInit() method... Overload me!");
	}

	/**
	 * Initialization code for teleop mode should go here.
	 *
	 * <p>Users should override this method for initialization code which will be called each time the
	 * robot enters teleop mode.
	 */
	public void teleopInit() {
		System.out.println("Default IterativeRobot.teleopInit() method... Overload me!");
	}

	/**
	 * Initialization code for physicalTest mode should go here.
	 *
	 * <p>Users should override this method for initialization code which will be called each time the
	 * robot enters physicalTest mode.
	 */
	@SuppressWarnings("PMD.JUnit4TestShouldUseTestAnnotation")
	public void testInit() {
		System.out.println("Default IterativeRobot.testInit() method... Overload me!");
	}

	/* ----------- Overridable periodic code ----------------- */

	private boolean m_rpFirstRun = true;

	/**
	 * Periodic code for all robot modes should go here.
	 *
	 * <p>This function is called each time a new packet is received from the driver station.
	 *
	 * <p>Packets are received approximately every 20ms.  Fixed loop timing is not guaranteed due to
	 * network timing variability and the function may not be called at all if the Driver Station is
	 * disconnected.  For most use cases the variable timing will not be an issue.  If your code does
	 * require guaranteed fixed periodic timing, consider using Notifier or PIDController instead.
	 */
	public void robotPeriodic() {
		if (m_rpFirstRun) {
			System.out.println("Default IterativeRobot.robotPeriodic() method... Overload me!");
			m_rpFirstRun = false;
		}
	}

	private boolean m_dpFirstRun = true;

	/**
	 * Periodic code for disabled mode should go here.
	 *
	 * <p>Users should override this method for code which will be called each time a new packet is
	 * received from the driver station and the robot is in disabled mode.
	 *
	 * <p>Packets are received approximately every 20ms.  Fixed loop timing is not guaranteed due to
	 * network timing variability and the function may not be called at all if the Driver Station is
	 * disconnected.  For most use cases the variable timing will not be an issue.  If your code does
	 * require guaranteed fixed periodic timing, consider using Notifier or PIDController instead.
	 */
	public void disabledPeriodic() {
		if (m_dpFirstRun) {
			System.out.println("Default IterativeRobot.disabledPeriodic() method... Overload me!");
			m_dpFirstRun = false;
		}
	}

	private boolean m_apFirstRun = true;

	/**
	 * Periodic code for autonomous mode should go here.
	 *
	 * <p>Users should override this method for code which will be called each time a new packet is
	 * received from the driver station and the robot is in autonomous mode.
	 *
	 * <p>Packets are received approximately every 20ms.  Fixed loop timing is not guaranteed due to
	 * network timing variability and the function may not be called at all if the Driver Station is
	 * disconnected.  For most use cases the variable timing will not be an issue.  If your code does
	 * require guaranteed fixed periodic timing, consider using Notifier or PIDController instead.
	 */
	public void autonomousPeriodic() {
		if (m_apFirstRun) {
			System.out.println("Default IterativeRobot.autonomousPeriodic() method... Overload me!");
			m_apFirstRun = false;
		}
	}

	private boolean m_tpFirstRun = true;

	/**
	 * Periodic code for teleop mode should go here.
	 *
	 * <p>Users should override this method for code which will be called each time a new packet is
	 * received from the driver station and the robot is in teleop mode.
	 *
	 * <p>Packets are received approximately every 20ms.  Fixed loop timing is not guaranteed due to
	 * network timing variability and the function may not be called at all if the Driver Station is
	 * disconnected.  For most use cases the variable timing will not be an issue.  If your code does
	 * require guaranteed fixed periodic timing, consider using Notifier or PIDController instead.
	 */
	public void teleopPeriodic() {
		if (m_tpFirstRun) {
			System.out.println("Default IterativeRobot.teleopPeriodic() method... Overload me!");
			m_tpFirstRun = false;
		}
	}

	private boolean m_tmpFirstRun = true;

	/**
	 * Periodic code for physicalTest mode should go here.
	 *
	 * <p>Users should override this method for code which will be called each time a new packet is
	 * received from the driver station and the robot is in physicalTest mode.
	 *
	 * <p>Packets are received approximately every 20ms.  Fixed loop timing is not guaranteed due to
	 * network timing variability and the function may not be called at all if the Driver Station is
	 * disconnected.  For most use cases the variable timing will not be an issue.  If your code does
	 * require guaranteed fixed periodic timing, consider using Notifier or PIDController instead.
	 */
	@SuppressWarnings("PMD.JUnit4TestShouldUseTestAnnotation")
	public void testPeriodic() {
		if (m_tmpFirstRun) {
			System.out.println("Default IterativeRobot.testPeriodic() method... Overload me!");
			m_tmpFirstRun = false;
		}
	}
}
