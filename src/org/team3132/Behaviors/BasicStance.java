package org.team3132.Behaviors;

import org.team3132.lib.util.SystemUtil;
import org.team3132.subsystems.Drivebase;

public class BasicStance {

    static Drivebase drivebase = Drivebase.getInstance();

    public static void tipForward() {
        drivebase.setPivots(0,0,-60,-60);
    }

    public static void standUp() {
        drivebase.setPivots(-45, -45, -45, -45);
    }

    public static void tipBack() {
        drivebase.setPivots(-60,-60,0,0);
    }

    public static void handsUp() {
        drivebase.setPivots(90, 90, 90, 90);
    }

    public static void tipLeft() {
        drivebase.setPivots(-5, -60, -5, -60);
    }

    public static void tipRight() {
        drivebase.setPivots(-60, -5, -60, -5);
    }

    public static void stairPrep() {
        drivebase.setPivots(80, 80, 170, 170);
    }

    public static void waitForPivots() {
        while (!drivebase.isPivotOnTarget()) {
            SystemUtil.delay(50);
        }
    }

    public static void pointFrontRight() {
        drivebase.setPivots(90, 45, 90, 90);
    }

    public static void surprised() {
        drivebase.setPivots(110, 110, 90, 90);
    }

}
