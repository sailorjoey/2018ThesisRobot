package org.team3132.lib.util;

public class RingCounter {
	
	int idx = 0;
	int size = 0;
	double[] data;
	
	double sum = 0;
	
	/**
	 * Creates a ring counter with size n
	 * @param size
	 */
	public RingCounter(int size) {
		this.size = size;
		data = new double[size];
	}
	
	/**
	 * Clears all stored data to 0s
	 */
	public synchronized void flush() {
		idx = 0;
		for(int i=0; i<data.length;i++) {
			data[i] = 0;
		}
		
		sum = 0;
	}
	
	/**
	 * Sets a new value to the ring counter, replacing the oldest value
	 * @param newValue the new value to add to the counter
	 */
	public synchronized void set(double newValue) {
		idx++;
		if(idx >= size)
			idx = 0;
		
		sum -= data[idx];
		sum += newValue;
		data[idx] = newValue;
	}
	
	/**
	 * Allows boolean use, stores as 0s and 1s
	 * @param newValue the new value to add to the counter
	 */
	public synchronized void set(boolean newValue) {
		if(newValue) {
			set(1);
		} else {
			set(0);
		}
	}
	
	/**
	 * Gets the last value added to the ring counter
	 * @return the last value added
	 */
	public synchronized double getLast() {
		return data[idx];
	}
	
	/**
	 * Returns the average of the values in the ring counter
	 * @return the average
	 */
	public synchronized double getAvg() {
		return sum / size;
	}
	
	/**
	 * Returns the sum of all elements in the ring counter
	 * @return the sum of all elements
	 */
	public synchronized double getTotalSum() {
		return sum;
	}
	
	public synchronized double getStandardDeviation() {
		double stdv = 0;
		double mean = getAvg();
		double mySum = 0;
		
		for(int i=0; i<data.length; i++) {
			mySum += (data[i] - mean)*(data[i] - mean);
		}
		stdv = Math.sqrt(mySum / size);
		
		return stdv;
	}
	
	/**
	 * Gets the entire data array
	 * @return the data storage array
	 */
	public synchronized double[] getData() {
		return data;
	}
}
