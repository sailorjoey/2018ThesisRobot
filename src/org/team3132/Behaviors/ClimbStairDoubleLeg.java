package org.team3132.Behaviors;

import org.team3132.lib.util.SystemUtil;
import org.team3132.subsystems.Drivebase;

public class ClimbStairDoubleLeg extends AutomatedRoutine {

    Drivebase drivebase = Drivebase.getInstance();

    @Override
    public void init() {
        //BasicStance.stairPrep();
        drivebase.setAutomated(true);
    }

    @Override
    public void run() {
        // approach stair
        drivebase.setPropulsions(0.3, 0.3, 0.3, 0.3);
        SystemUtil.delay(500);

        waitForPivots();
        // lift front up first stair
        drivebase.setFrontPivots(-30, -1);
        waitForPivots();

        // turn off drive
        drivebase.setPropulsions(0,0,0,0);

        // lift up back and reset front
        drivebase.setPivots(90, 1, 90, 1, -50, 1, -50, 1);
        waitForPivots();

        // turn on drive and lift front up second stair
        drivebase.setPropulsions(0.3, 0.3, 0.3, 0.3);
        drivebase.setFrontPivots(-80, -1);
        waitForPivots();
        //SystemUtil.delay(500);

        drivebase.setPropulsions(0, 0, 0.3, 0.3);

        drivebase.setBackPivots(200, 1);
        waitForPivots();
        drivebase.setPropulsions(0.1,0.1,0.1,0.1);

        drivebase.setBackPivots(235, 1);
        waitForPivots();

        drivebase.setPropulsions(0, 0, 0, 0);

        drivebase.setFrontPivots(-30, -1);
        waitForPivots();

        //drivebase.setBackPivots(-100, 1);
        waitForPivots();




        waitForPivots();
        // turn off drive
        drivebase.setPropulsions(0, 0, 0, 0);

        drivebase.setAutomated(false);
    }

    @Override
    public void kill() {
        drivebase.cleanup();
    }

    public void waitForPivots() {
        while (!drivebase.isPivotOnTarget()) {
            SystemUtil.delay(50);
        }

        //SystemUtil.delay(500);
    }
}
