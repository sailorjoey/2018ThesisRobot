package org.team3132.Behaviors;

import org.team3132.lib.util.SystemUtil;
import org.team3132.subsystems.Drivebase;

public class WaggleArms extends AutomatedRoutine {

    Drivebase drivebase = Drivebase.getInstance();

    @Override
    public void init() {

    }

    @Override
    public void run() {
        for(int i=0; i<2; i++) {
            drivebase.setPivots(85, 95, 95, 85);
            SystemUtil.delay(100);
            drivebase.setPivots(95,85, 85, 95);
            SystemUtil.delay(100);
        }
        drivebase.setPivots(90, 90, 90, 90);
    }

    @Override
    public void kill() {
        drivebase.setPivots(90, 90, 90, 90);
    }
}
