package org.team3132.Behaviors;

public abstract class AutomatedRoutine implements Runnable {

    public abstract void init();

    /**
     * Run the routine
     */
    public abstract void run();

    /**
     * Used to kill the routine
     */
    public abstract void kill();
}
