package org.team3132.lib;


/**
 * Subsystem Class - All Subsystems should extend this.
 *
 * This class offers elements that should be common across all subsystems. Each subsystem should make use of enable/disable, and all calls to the motors should be made in update.
 * The subsystem should only be responsible for making robot-critical decisions, such as not moving a piece to prevent the robot eating itself.
 */
public abstract class Subsystem implements DashboardUpdater {
	protected String name;
	protected boolean present = true;
	protected boolean enabled = true;
	protected boolean stayAlive = true;
	protected long delayTime = 10;	// ms between runs

	protected boolean automated = false;
	
	public Subsystem(String name) {
		this.name = name;
	}
	
	public void enable() {
		enabled = true;
	}
	
	public void disable() {
		enabled = false;
		cleanup();
	}
	
	public void exit() {
		stayAlive = false;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public boolean isPresent() {
		return present;
	}
	
	public String getName() {
		return name;
	}

	public void setAutomated(boolean automated) {
		this.automated = automated;
	}

	public boolean isAutomated() {
		return automated;
	}
	
	/**
	 * This method is used to do a non moving health check of the system. it runs on robotInit()
	 * @return true if the system is OK, false if there is an error
	 */
	public abstract boolean initCheck();
	
	/**
	 * This method is used to a moving health check of the system. it runs during tests
	 * @return true if the system is OK, false if there is an error
	 */
	public abstract boolean runningCheck();

	/**
	 * This is used to deal with auto being killed and the subsystems being disabled.
	 */
	public abstract void cleanup();


	public abstract void updateSmartDashboard();
}
