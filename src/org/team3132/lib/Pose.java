package org.team3132.lib;

import org.team3132.lib.util.AngleUtil;

import edu.wpi.first.wpilibj.Timer;

/*
 *  Distances are in inches. Angles are always in degrees.
 */
public class Pose {
	public double x;		// +ve is to the right of the robot in inches.
	public double y;		// -ve is forwards in inches.
	public double heading;  // Angle in degrees from initial direction (can be positive or negative and multiple turns)
    public double speed;	// current speed (inches/second)
    public double timeSec;		// time of this location (ms after start)
    
    public Pose(double x, double y) {
    	this(x, y, 0, 0, Timer.getFPGATimestamp());
	}
    
    public Pose(double x, double y, double headingDegrees) {
    	this(x, y, headingDegrees, 0, Timer.getFPGATimestamp());
	}
    
    public Pose(double x, double y, double headingDegrees, double speed) {
    	this(x, y, headingDegrees, speed, Timer.getFPGATimestamp());
	}   
    
    public Pose(double x, double y, double headingDegrees, double speed, double time) {
    	this.x = x;
    	this.y = y;
    	this.heading = headingDegrees;
    	this.speed = speed;
    	this.timeSec = time;
	}

	// Take two field oriented positions and returns a location relative to the second location.
    public Pose getRelativeToLocation(Pose otherLocation) {
        // Subtract off the robots position from the position.
        double newX = x - otherLocation.x;
        double newY = y - otherLocation.y;
        // Rotate to match the robots orientation.
        double result[] = AngleUtil.rotateVector(newX, newY, -otherLocation.heading);
        // Subtract off the robots angle from the original angle so the angle is also relative.
        Pose loc = new Pose(result[0], result[1], heading - otherLocation.heading, 0, 0);
        return loc;
    }
    
    @Override
    public String toString() {
        return String.format("X(%.3f),Y(%.3f),H(%.3f),S(%3f)", x, y, heading, speed);
    }
    
    public void copyFrom(Pose other) {
        x = other.x;
        y = other.y;
        heading = other.heading;
    }
    
    public final Pose addVector(double distance, double angle) {
    	/*
    	 * Change a location by the vector supplied.
    	 * The final heading is aligned with the vector
    	 * 
    	 * Angle is in degrees.
    	 */
    	double newX = distance * AngleUtil.sin(angle);
    	double newY = distance * AngleUtil.cos(angle);
    	return new Pose(x + newX, y - newY, angle);
    }
    
    /**
     * Return the bearing from this pose to the destination pose. The bearing is from -180 to 180 degrees.
     * If the destination has the same X and Y co-ordinates the bearing is defined as the destination heading normalised.
     * @param dest
     * @return
     */
	public double bearingTo(Pose dest) {
		double angle;
		double diffX = x - dest.x;
		double diffY = y - dest.y;
		if (diffY == 0) {
			// can't calculate atan on a vertical trajectory
			if (diffX > 0.0) {
				angle = 90;
			} else if (diffX < 0.0) {
				angle = -90;
			} else {	// we aren't moving - return the normalised destination direction
				angle = AngleUtil.normalise(dest.heading, 360);
			}
		} else {
			// Question: Should we be using atan2() instead?
			angle = -AngleUtil.atan(diffX/diffY);
			// there are four quadrants, we have to treat each differently
			if (diffY <= 0) {
				if (diffX >= 0.0) {
					angle = angle - 180;
				} else {
					angle = angle + 180;
				}
			}
		}
		return angle;
	}

	public double distanceTo(Pose dest) {
		double diffX = dest.x - x;
		double diffY = dest.y - y;
		// See, learning about Pythagoras was useful :)
		return Math.sqrt((diffX * diffX) + (diffY * diffY));
	}
	
	/**
	 * Calculate the angle between two poses
	 * @param dest the destination angle as the heading value
	 * @return the normalised angle between the two headings
	 */
	public double angleBetweenBearings(Pose dest) {
		return Math.abs(AngleUtil.normalise(dest.heading - heading, 360));
	}
	
	public String getDygraphHeader(String name) {
		return String.format("%1$s/X,%1$s/Y,%1$s/Heading,%1$s/Speed,", name);
	}
	
	public String getDygraphData() {
		return String.format("%f,%f,%f,%f", x, y, heading, speed);
	}
}
