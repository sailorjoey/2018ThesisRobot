package org.team3132.lib;

/*
 * Class to calculate the position (and heading) of a bezier spline.
 * We are given the four enclosing points and the "distance" through the spline.
 * We return the point and heading
 */
public class BezierSplineCubic {
	
	private static Pose splitLine(Pose start, Pose end, double distance) {
		double newX, newY;
		
		if (distance > 1.0) {
			// error - distance must be within 0.0 to 1.0
			distance = 1.0;
		} else if (distance < 0.0) {
			distance = 0.0;
		}
		newX = ((end.x - start.x) * distance) + start.x;
		newY = ((end.y - start.y) * distance) + start.y;
		return new Pose(newX, newY);
	}
	
	public static Pose findBezierSplineCubic(Pose start, Pose control1, Pose control2, Pose end, double distance) {
		/*
		 *  return the Pose (X,Y) and heading of a point that is distance (0.0 to 1.0) through the
		 *  Cubic Bezier Spline as specified.
		 */
		Pose Q0, Q1, Q2;
		Pose R0, R1;
		Pose P;
		
		Q0 = splitLine(start, control1, distance);
		Q1 = splitLine(control1, control2, distance);
		Q2 = splitLine(control2, end, distance);
		R0 = splitLine(Q0, Q1, distance);
		R1 = splitLine(Q1, Q2, distance);
		P = splitLine(R0, R1, distance);
		P.heading = R0.bearingTo(R1);	// and calculate the heading between the two points
		return P;
	}
}
