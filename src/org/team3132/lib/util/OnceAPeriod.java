package org.team3132.lib.util;

import edu.wpi.first.wpilibj.Timer;

public class OnceAPeriod {
	private double last;
	private double timeout;
	
	public OnceAPeriod() {
		this(1.0);
	}
	
	public OnceAPeriod(double timeout) {
		this.timeout = timeout;
		last = Timer.getFPGATimestamp();
	}
	
	public boolean isTimeOut() {
		double now = Timer.getFPGATimestamp();
		if (now - last < timeout) {
			return false;
		}
		last = now;
		return true;
	}
}
