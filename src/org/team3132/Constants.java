package org.team3132;


import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * These are constants used by the robot. They define physical things about the world, or the robot.
 * 
 * We collate them here to have all these stored in one place.
 */
public class Constants {
	
	public static final int PCM_CAN_ID = 61;
	
	/*
	 *  Drivebase constants
	 *
	 * The robot has two motors on each side.
	 * These motors are controlled by CAN bus based TALON motor controllers. There is one master and one slave on each side.
	 * Both are configured identically, so forward is the same voltage on each.
	 */
	public static final int DRIVE_LEFT_FRONT_PROPULSION_CAN_ID = 6;			// front left propulsion motor (CAN ID)
	public static final int DRIVE_LEFT_FRONT_PIVOT_CAN_ID = 5;			// front left pivot motor (CAN ID)
	public static final int DRIVE_LEFT_BACK_PROPULSION_CAN_ID = 7;			// front left propulsion motor (CAN ID)
	public static final int DRIVE_LEFT_BACK_PIVOT_CAN_ID = 8;			// front left pivot motor (CAN ID)
	public static final int DRIVE_RIGHT_FRONT_PROPULSION_CAN_ID = 2;			// front left propulsion motor (CAN ID)
	public static final int DRIVE_RIGHT_FRONT_PIVOT_CAN_ID = 1;			// front left pivot motor (CAN ID)
	public static final int DRIVE_RIGHT_BACK_PROPULSION_CAN_ID = 3;			// front left propulsion motor (CAN ID)
	public static final int DRIVE_RIGHT_BACK_PIVOT_CAN_ID = 4;			// front left pivot motor (CAN ID)

	public static final double WHEEL_POD_PIVOT_TICKS_PER_DEG = 4096.0 / 360.0; // ticks per degree

	public static final int DRIVE_LEFT_FRONT_0_POS = 1487;
    public static final int DRIVE_LEFT_BACK_0_POS = 4096-3126;
    public static final int DRIVE_RIGHT_FRONT_0_POS = 3076;
    public static final int DRIVE_RIGHT_BACK_0_POS = 4096-1312;

    public static final boolean DRIVE_LF_PIVOT_PHASE = false;
    public static final boolean DRIVE_LF_PROP_PHASE = false;
    public static final boolean DRIVE_LB_PIVOT_PHASE = true;
    public static final boolean DRIVE_LB_PROP_PHASE = false;
    public static final boolean DRIVE_RF_PIVOT_PHASE = false;
    public static final boolean DRIVE_RF_PROP_PHASE = false;
    public static final boolean DRIVE_RB_PIVOT_PHASE = true;
    public static final boolean DRIVE_RB_PROP_PHASE = false;

    public static final double DRIVE_PIVOT_KP = 15;
	public static final double DRIVE_PIVOT_KI = 0.0001;
	public static final double DRIVE_PIVOT_KD = 0;
	public static final double DRIVE_PIVOT_KF = 0;

    public static final double DRIVE_PROPULSION_KP = 0.3;
    public static final double DRIVE_PROPULSION_KI = 0;
    public static final double DRIVE_PROPULSION_KD = 3;
    public static final double DRIVE_PROPULSION_KF = 0.25;

	public static final double PIVOT_TOLERANCE = 2;
	public static final double PROPULSION_SPEED_MAX = 5000;
	public static boolean PROPULSION_VELOCITY_MODE = false;



	// logging information constants
	public static final String WEB_BASE_PATH = "/media/sda1";		// where web server's data lives
	public static final String LOG_BASE_PATH = WEB_BASE_PATH;		// log files (has to be inside web server)
	public static final String LOG_DATA_EXTENSION = "data";
	public static final String LOG_DATE_EXTENSION = "date";
	public static final Path LOG_NUMBER_FILE = Paths.get(System.getProperty("user.home"), "lognumber.txt");
	public static final int	 WEB_PORT = 5800;			// first open port for graph/log web server
	public static final double LOG_GRAPH_PERIOD = 0.05;	// run the graph updater every 50ms
}
