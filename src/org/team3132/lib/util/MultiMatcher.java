package org.team3132.lib.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Tool for combining the power of multiple Matchers.
 */
public class MultiMatcher {
    /*Internal Variables*/
    private ArrayList<Matcher> matchers = new ArrayList<>();
    private int findCount;

    /*Constructors*/
    public MultiMatcher(Matcher matcher) {
        matchers.add(matcher);
        updateFindCount();
        resetAll();
    }

    public MultiMatcher(Matcher... matchers) {
        Collections.addAll(this.matchers, matchers);
        updateFindCount();
        resetAll();
    }

    /**
     * Re-runs each matcher and updates the find count
     * @return Find Count
     */
    public int updateFindCount() {
        int finds = 0;
        resetAll();
        for (Matcher matcher : matchers) {
            while (matcher.find()) finds++;
        }
        this.findCount = finds;
        return finds;
    }

    /**
     * Retrieves the find count
     * @return Find Count
     */
    public int getFindCount() {
        return findCount;
    }


    /**
     * Internal method to reset all Matcher's
     */
    private void resetAll() {
        for (Matcher matcher : matchers) {
            matcher.reset();
        }
    }

    /**
     * Resets all Matchers and updates find counts
     */
    public MultiMatcher reset() {
        updateFindCount();
        resetAll();
        return this;
    }

    /**
     * Resets the Matcher's with the specified Pattern
     * @param parentPattern Pattern of Matcher
     */
    public MultiMatcher reset(Pattern parentPattern) {
        updateFindCount();
        for (Matcher matcher : matchers) {
            if (matcher.pattern().equals(parentPattern)) {
                matcher.reset();
            }
        }
        return this;
    }

    /**
     * Resets all Matchers and changes their String
     * @param newString New String to use
     */
    public MultiMatcher reset(String newString) {
        for (Matcher matcher : matchers) {
            matcher.reset(newString);
        }
        updateFindCount();
        resetAll();
        return this;
    }

    /**
     * Reset's the Matcher's with the specifed Pattern and changes their String
     * @param parentPattern Pattern of Matcher
     * @param newString New String to use
     */
    public MultiMatcher reset(Pattern parentPattern, String newString) {
        for (Matcher matcher : matchers) {
            if (matcher.pattern().equals(parentPattern)) {
                matcher.reset(newString);
            }
        }
        updateFindCount();
        for (Matcher matcher : matchers) {
            if (matcher.pattern().equals(parentPattern)) {
                matcher.reset();
            }
        }
        return this;
    }

    /**
     * Checks if any Matcher finds it's Pattern
     * @return If any Matcher found it's Pattern
     */
    public boolean findAll() {
        resetAll();
        boolean found = false;
        for (Matcher matcher : matchers) {
            if (matcher.find()) found = true;
        }
        return found;
    }

    /**
     * Calls find() on each Matcher
     * @return If any matcher found it's Pattern
     */
    public boolean findNext() {
        boolean found = false;
        for (Matcher matcher : matchers) {
            if (matcher.find()) found = true;
        }
        return found;
    }

    /**
     * Calls find() on the Matcher with the specified Pattern
     * @param parentPattern Pattern of Matcher
     * @return Result of find()
     */
    public boolean findSpecific(Pattern parentPattern) {
        return getMatcher(parentPattern).find();
    }

    /**
     * Get't the start of a specific Matcher
     * @param parentPattern Pattern of Matcher
     * @return Result of start()
     */
    public int getStartSpecific(Pattern parentPattern) {
        return getMatcher(parentPattern).start();
    }

    /**
     * Get't the end of a specific Matcher
     * @param parentPattern Pattern of Matcher
     * @return Result of end()
     */
    public int getEndSpecific(Pattern parentPattern) {
        return getMatcher(parentPattern).end();
    }

    /**
     * Get's the start of all Matcher's
     * @return The starts
     */
    public int[] getStarts() {
        int[] starts = new int[matchers.size()];
        for (int i = 0; i < matchers.size(); i++) {
            if (matchers.get(i).find()) {
                starts[i] = matchers.get(i).start();
            }
        }
        return starts;
    }

    /**
     * Get's the end of all Matcher's
     * @return The ends
     */
    public int[] getEnds() {
        int[] ends = new int[matchers.size()];
        for (int i = 0; i < matchers.size(); i++) {
            if (matchers.get(i).find()) {
                ends[i] = matchers.get(i).end();
            }
        }
        return ends;
    }

    /**
     * Reset's each Matcher, and finds every start
     * @return All the starts
     */
    public int[] getAllStarts() {
        int[] starts = new int[updateFindCount()];
        resetAll();
        int pos = 0;
        for (int i = 0; i < matchers.size(); i++) {
            while (matchers.get(i).find()) {
                starts[pos] = matchers.get(i).start();
                pos++;
            }
        }

        return NumberArrayUtil.sort(starts);
    }

    /**
     * Reset's each Matcher, and finds every end
     * @return ALl the ends
     */
    public int[] getAllEnds() {
        int[] ends = new int[updateFindCount()];
        resetAll();
        int pos = 0;
        for (int i = 0; i < matchers.size(); i++) {
            while (matchers.get(i).find()) {
                ends[pos] = matchers.get(i).end();
                pos++;
            }
        }
        return NumberArrayUtil.sort(ends);
    }

    /**
     * Get's the first start of any Matcher
     * @return The first start
     */
    public int getFirstStart() {
        int start = Integer.MAX_VALUE;
        for (Matcher matcher : matchers) {
            matcher.reset();
            if (matcher.find() && matcher.start() < start) start = matcher.start();
        }
        return start != Integer.MAX_VALUE ? start : -1;
    }

    /**
     * Retrieves the specified Matcher
     * @param parentPattern Pattern of Matcher
     * @return Specified Matcher
     */
    public Matcher getMatcher(Pattern parentPattern) {
        for (Matcher matcher : matchers) {
            if (matcher.pattern().equals(parentPattern)) return matcher;
        }

        return null;
    }
}
