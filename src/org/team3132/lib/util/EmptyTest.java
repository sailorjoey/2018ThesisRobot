package org.team3132.lib.util;

import org.team3132.lib.Test;

public class EmptyTest implements Test {


    @Override
    public void testInit() {
        System.out.println("Empty Test: init");
    }

    @Override
    public void testPeriodic() {
        System.out.println("Empty Test: periodic");
    }

    @Override
    public void testEnd() {
        System.out.println("Empty Test: end");
    }
}
