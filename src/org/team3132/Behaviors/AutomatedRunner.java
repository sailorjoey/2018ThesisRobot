package org.team3132.Behaviors;

import org.team3132.Robot;
import org.team3132.lib.util.SystemUtil;

public class AutomatedRunner {

    private AutomatedRoutine routine = new EmptyAutomated();
    private Thread thread;

    public void setRoutine(AutomatedRoutine newRoutine) {
        this.routine = newRoutine;
    }

    public void start() {
        if (thread == null || !thread.isAlive()) {
            thread = new Thread(routine);
            routine.init();
            thread.start();
        }
    }

    public boolean isRunning() {
        if (thread == null) return false;
        return (thread.isAlive());
    }

    // More details about why thread.stop() is deprecated (and why it's a bad idea): http://stackoverflow.com/questions/16504140/thread-stop-deprecated
    @SuppressWarnings("deprecation")
    public void kill() {
        Robot.log.sub("automated runner: killing automated routine");
        if (thread == null) {
            return;
        }

        try {
            thread.stop();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        thread = null;

        SystemUtil.delay(10);
        this.routine.kill();
    }

    public void reset() {
        thread = null;
    }
}
