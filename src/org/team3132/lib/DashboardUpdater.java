package org.team3132.lib;

public interface DashboardUpdater {

	/**
	 * Called when this should update the smartdashboard.
	 */
	void updateSmartDashboard();
}
