package org.team3132.lib.util;



/**
 * Created by sailo on 5/9/2016.
 */
public class MathUtil {

    /**
     *  Scales the value in to the specified range
     * @param valueIn the value to be scaled
     * @param baseMin the minimum input value
     * @param baseMax the maximum input value
     * @param limitMin the scaled minimum output
     * @param limitMax the scaled maximum output
     * @return the scaled value
     */
    public static double scale(final double valueIn, final double baseMin, final double baseMax, final double limitMin, final double limitMax) {
        return ((limitMax - limitMin) * (valueIn - baseMin) / (baseMax - baseMin)) + limitMin;
    }

    /**
     * Scales the value in to the specified range and limits the output to the max and min specified
     * @param valueIn the value to be scaled
     * @param baseMin the minimum input value
     * @param baseMax the maximum input value
     * @param limitMin the scaled minimum output
     * @param limitMax the scaled maximum output
     * @return the scaled value
     */
    public static double scaleClipped(final double valueIn, final double baseMin, final double baseMax, final double limitMin, final double limitMax) {
        double calculatedValue = ((limitMax - limitMin) * (valueIn - baseMin) / (baseMax - baseMin)) + limitMin;
        return limitValue(calculatedValue, limitMax, limitMin);
    }

    /**
     * Scales the input value to the standard range for a joystick value. Used often enough to be handy
     * @param valueIn typically a joystick axis
     * @return a scaled value from -1.0 to 1.0
     */
    public static double scaleClipped(final double valueIn){
        return scaleClipped(valueIn, -1.27, 1.28, -1.0, 1.0);
    }

    /**
     * Limits the input value between -1.0 and 1.0
     * @param val the value to be limited
     * @return the limited value
     */
    public static double limitValue(double val) {
        return limitValue(val, 1.0);
    }

    /**
     * Limits the input value between -max to +max
     * @param val the value to be limited
     * @param max the absolute value of the limit
     * @return the limited value
     */
    public static double limitValue(double val, double max) {
        if(val > max) {
            return max;
        } else if(val < -max) {
            return -max;
        } else {
            return val;
        }
    }

    /**
     * Limits the input value between the input min and input max
     * @param val the value to be limited
     * @param max the maximum output value
     * @param min the minimum output value
     * @return the limited value
     */
    public static double limitValue(double val, double max, double min) {
        if(val > max) {
            return max;
        } else if(val < min) {
            return min;
        } else {
            return val;
        }
    }

    /**
     * Squares the input value while keeping the +/- sign
     * @param val the value to be squared
     * @return the squared value
     */
    public static double squareMaintainSign(double val) {
        double output = val * val;

        //was originally negative
        if(val < 0) {
            output = -output;
        }

        return output;
    }

    /**
     * Cubes the input value while keeping the +/- sign
     * @param val the value to be cubed
     * @return the cubed value
     */
    public static double power3MaintainSign(double val){
        double output = val*val*val;
        return output;
    }

    /**
     * Returns the maximum value of the three passed in
     * @param a input 1
     * @param b input 2
     * @param c input 3
     * @return the max of a, b, c
     */
    public static double max(double a, double b, double c) {
        a = Math.abs(a);
        b = Math.abs(b);
        c = Math.abs(c);
        if(a > b && a > c) {
            return a;
        } else if(b > c) {
            return b;
        } else {
            return c;
        }
    }
    
    /*
    
    Code to physicalTest the below functions.
    
    public class test1 {
	public static void main(String[] args) {
		forwards();
		backwards();
	}
	
	public static void forwards() {
		double distance = 10.0;
		double initialSpeed = 0.1;
		double finalSpeed = 0.2;
		double cruiseSpeed = 1.0;	
		double accel = 0.6;
		double decel = -0.2;
		int points = 300;
		
		for (int i = -10; i <= points+10; i++) {
			double d = distance * i / ((double)points);
			double as = MathUtil.calculateAccelRampSpeed(initialSpeed, cruiseSpeed, accel, d);
			double ds = MathUtil.calculateDecelRampSpeed(cruiseSpeed, finalSpeed, decel, distance - d);
		    double cs = MathUtil.calculateTrapSpeedAtPosition(initialSpeed, cruiseSpeed, finalSpeed, accel, decel, distance, d); // single line

			System.out.printf("distance: %f [%f, %f] speed: %f = %f\n", d, as, ds, Math.min(as, ds), cs);
		}
	}
	
	public static void backwards() {
		double distance = -10.0;
		double initialSpeed = -0.1;
		double finalSpeed = -0.2;
		double cruiseSpeed = -1.0;	
		double accel = -0.6;
		double decel = 0.2;
		int points = 300;
		
		for (int i = -10; i <= points+10; i++) {
			double d = distance * i / ((double)points);
			double as = MathUtil.calculateAccelRampSpeed(initialSpeed, cruiseSpeed, accel, d);
			double ds = MathUtil.calculateDecelRampSpeed(cruiseSpeed, finalSpeed, decel, distance - d);
		    double cs = MathUtil.calculateTrapSpeedAtPosition(initialSpeed, cruiseSpeed, finalSpeed, accel, decel, distance, d); // single line
			System.out.printf("distance: %f [%f, %f] speed: %f = %f\n", d, as, ds, Math.max(as, ds), cs);
		}
	}

    
    */
    
    /**
     * Helper routine to calculate the speed along a straight line.
     * We calculate the speed so that we follow a ramped maximum speed.
     * We start at initialSpeed, accelerate to maxSpeed, cruise at this speed and then finally decelerate until we are at finalSpeed at distance away.
     * This routine calculate the speed at position, being a point from 0 to distance away.
     * @param initialSpeed current speed
     * @param maxSpeed maximum, or cruising speed
     * @param finalSpeed speed at the end of the movement
     * @param accel acceleration rate
     * @param decel deceleration rate
     * @param distance distance that we wish to end away after the movement
     * @param position position we are at on the movement (same sign as distance)
     * @return Speed that the robot should be travelling 
     */
    public static double calculateTrapSpeedAtPosition(double initialSpeed, double maxSpeed, double finalSpeed, double accel, double decel, double distance, double position) {
		double as = MathUtil.calculateAccelRampSpeed(initialSpeed, maxSpeed, accel, position);
		double ds = MathUtil.calculateDecelRampSpeed(maxSpeed, finalSpeed, decel, distance - position);
		if (distance < 0) {
			if (position > 0) return initialSpeed;
			if (position <= distance) return finalSpeed;
			return Math.max(as, ds);
		}
		if (position < 0) return initialSpeed;
		if (position >= distance) return finalSpeed;
		return Math.min(as, ds);
    }
    
    /**
     * Return the speed required for a ramp that starts at topSpeed and will drop to finalSpeed with a deceleration of decel. We are currently at
     * distance away from the final point. The deceleration rate is decel. We want the speed at distance from the end point.
     * 				|\
     * 				| \ decel
     * 	topSpeed----+--\
     * 				|   \
     * 		distance|    \_____ finalSpeed
     * @param topSpeed The speed we are running at at the beginning of the ramp.
     * @param finalSpeed The speed we wish to be travelling at at the end of the ramp
     * @param decel The deceleration rate in distance(inches)/time(seconds)
     * @param distance The distance we are from the destination
     * @return
     */
    public static double calculateDecelRampSpeed(double topSpeed, double finalSpeed, double decel, double distance) {
    	if (distance == 0.0) return finalSpeed;
    	if (distance < 0.0) {
    		return -Math.min(calculateRampSpeed(-finalSpeed, decel, -distance), -topSpeed);
    	}
    	return Math.min(calculateRampSpeed(finalSpeed, -decel, distance), topSpeed);
    }
	
    /**
     * Return the speed required for a ramp that starts at initialSpeed and will climb. It has a maximum of maxSpeed at which point the
     * speed stays constant. The acceleration rate is accel. We want the speed at distance from the start point.
     * 						 /|
     * 				accel	/ |
     * 					   /--+---------- max Speed
     * 					  /   |
     * initialSpeed _____/    |distance
     * 
     * @param initialSpeed The speed we were travelling when the ramp started
     * @param maxSpeed The maximum speed we can go. We top out the ramp at this speed
     * @param accel The acceleration is distance(inches)/time(seconds)
     * @param distance the distance we are currently from the start
     * @return speed that we should be travelling at at distance from the end of the ramp.
     */
    public static double calculateAccelRampSpeed(double initialSpeed, double maxSpeed, double accel, double distance) {
    	if (distance == 0.0) return initialSpeed;
    	if (distance < 0.0) {
    		return -Math.min(calculateRampSpeed(-initialSpeed, -accel, -distance), -maxSpeed);
    	}
    	return Math.min(calculateRampSpeed(initialSpeed, accel, distance), maxSpeed);
    }
    
    private static double calculateRampSpeed(double initialSpeed, double accel, double distance) {
		/*
		 * This is a helper routine to calculate the speed on a ramp. We can use the calculations for
		 * both acceleration or deceleration. For deceleration 
		 * 
		 * We are given four values:
		 * The initial speed: This is the speed we start the manoeuvre at. For acceleration we will
		 * move away from this speed immediately. For deceleration we will end at this speed.
		 * The maxSpeed: This is the maximum speed we are allowed to travel at. If we attempt go above this speed we are capped.
		 * The acceleration. This is the change in speed per unit time.
		 *  
		 *  We can do some fancy maths to calculate the speed on each of the accel and decel ramps at a distance:
		 *  given constant accel/decel
		 * 			final velocity = sqrt(inital velocity squared + 2 * accel * distance)
		 * 			Vf = (Vi^2 + 2*a*d)^(1/2)
		 * 
		 * OR
		 * 
		 *			distance = average velocity * time				d = 1/2*(Vf + Vi)*t
		 *  time = difference in velocities over acceleration.		t = (Vf - Vi)/a
		 *  
		 *  This means we can calculate the velocity at a distance by using the formula:
		 *  Vi = (Vf^2 - 2ad)^(1/2)
		 *  
		 *  Note, because of the square root here we may need to have the negative root if the distance is negative.
		 *  We can calculate this by using relative distance and headings to the origin and destination, but that's just
		 *  complicating the problem. I.e. final speed is an absolute in the direction we're travelling.
		 */
    	return Math.sqrt(initialSpeed * initialSpeed + 2 * accel * distance);
	}
}
