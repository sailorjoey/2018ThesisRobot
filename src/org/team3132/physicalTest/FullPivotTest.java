package org.team3132.physicalTest;

import edu.wpi.first.wpilibj.Joystick;
import org.team3132.lib.GamepadButtonsX;
import org.team3132.lib.Test;
import org.team3132.subsystems.Drivebase;

public class FullPivotTest implements Test {

    Drivebase drivebase = Drivebase.getInstance();
    Joystick stick = new Joystick(0);

    double targetAngle = 0;

    @Override
    public void testInit() {
        targetAngle = 0;

    }

    @Override
    public void testPeriodic() {
        if(stick.getRawButton(GamepadButtonsX.A_BUTTON)) {
            targetAngle = -90;
        } else if(stick.getRawButton(GamepadButtonsX.B_BUTTON)) {
            targetAngle = 180;
        } else if(stick.getRawButton(GamepadButtonsX.X_BUTTON)) {
            targetAngle = 0;
        } else if(stick.getRawButton(GamepadButtonsX.Y_BUTTON)) {
            targetAngle = 90;
        }

        if(stick.getPOV() == 0) {
            targetAngle += 5;
        } else if(stick.getPOV() == 180) {
            targetAngle -= 5;
        }


        drivebase.setPivots(targetAngle, targetAngle, targetAngle, targetAngle);
        System.out.println("target angle: " + targetAngle);
    }

    @Override
    public void testEnd() {

    }
}
