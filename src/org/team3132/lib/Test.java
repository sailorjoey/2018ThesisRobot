package org.team3132.lib;

public interface Test {
	
	/**
	 * Called once at the beginning of the physicalTest
	 */
	public void testInit();
	
	/**
	 * Called repeatedly during the physicalTest
	 */
	public void testPeriodic();
	
	/**
	 * Called once on physicalTest completion/end
	 */
	public void testEnd();
}
