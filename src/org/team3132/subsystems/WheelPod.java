package org.team3132.subsystems;

import com.ctre.phoenix.ParamEnum;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.team3132.Constants;
import org.team3132.Robot;
import org.team3132.lib.util.TalonSRXFactory;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

public class WheelPod {
	
	String name = "";
	TalonSRX mPropulsion;
	TalonSRX mPivot;


	int pivotDirection = 1;
	int pivotOffset;

	int pivotOnTargetCounter = 0;
	int pivotOnTargetMax = 5;

	public WheelPod(String name, int propulsionID, int pivotID, int pivotOffset, boolean propulsionSensorPhase, boolean pivotSensorPhase) {
		this.name = name;
		this.pivotOffset = pivotOffset;

        System.out.println("Wheel pod " + name + " created");

		mPropulsion = TalonSRXFactory.createDefaultTalon(propulsionID);
		mPropulsion.configSelectedFeedbackSensor(FeedbackDevice.QuadEncoder,0,10);
		mPropulsion.setNeutralMode(NeutralMode.Coast);
		mPropulsion.setSensorPhase(propulsionSensorPhase);
		mPropulsion.configSetParameter(ParamEnum.eClearPositionOnIdx,0,1,0, 10);
        mPropulsion.config_kP(0,Constants.DRIVE_PROPULSION_KP, 10);
        mPropulsion.config_kI(0,Constants.DRIVE_PROPULSION_KI, 10);
        mPropulsion.config_kD(0,Constants.DRIVE_PROPULSION_KD, 10);
        mPropulsion.config_kF(0,Constants.DRIVE_PROPULSION_KF, 10);


		mPivot = TalonSRXFactory.createDefaultTalon(pivotID);
		mPivot.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 10);
		mPivot.setNeutralMode(NeutralMode.Coast);
		mPivot.setSensorPhase(pivotSensorPhase);
		mPivot.config_kP(0,Constants.DRIVE_PIVOT_KP, 10);
        mPivot.config_kI(0,Constants.DRIVE_PIVOT_KI, 10);
        mPivot.config_kD(0,Constants.DRIVE_PIVOT_KD, 10);
        mPivot.config_kF(0,Constants.DRIVE_PIVOT_KF, 10);



        Robot.log.register(true, this::getPivotAngle,name + "/pivotAngle")
                .register(true, this::getPivotAngleTarget, name + "/pivotAngleTarget")
                .register(true, this::getPivotAngleAbsolute, name + "/pivotAngleAbsolute")
                .register(true, this::getPivotAngleTargetAbsolute, name + "/pivotAngleTargetAbsolute")
                .register(true, this::getPropulsionVelocityTarget, name + "/propulsionVelocityTarget")
                .register(true, this::getPropulsionPosition, name + "/propulsionPosition")
                .register(true, this::getPropulsionVelocity, name + "/propulsionVelocity")
                .register(true, () -> this.isInMecanum()?1:0, name + "/isInMecanum")
                .register(true, () -> this.getQuadrant(getPivotAngle()), name + "/quadrant");

    }
	
	public void setPropulsionSpeed(double speed) {

	    if(Constants.PROPULSION_VELOCITY_MODE) {
            speed = speed * Constants.PROPULSION_SPEED_MAX;
        }

	    if(Math.abs(speed) <= 1) {
            mPropulsion.set(ControlMode.PercentOutput, speed);
        } else {
	        mPropulsion.set(ControlMode.Velocity, speed);
        }


	}
	
	public void setPivotAngleTarget(double angle) {
        angle = (angle * Constants.WHEEL_POD_PIVOT_TICKS_PER_DEG * pivotDirection) + pivotOffset;
        mPivot.set(ControlMode.Position, angle);
	}

	public void setPivotAngleTargetAbsolute(double angle, int direction) {
        double current = getPivotAngleTarget();
        double target = angle % 360;
        double setpoint = current;
        double error = (target - getPivotAngleTargetAbsolute()) % 360;
        if(error%360 == 0) {
            error = 0;
        }


        // chooses direction to go based on current and target quadrants
        if(direction == 0) {
            int currentQuad = getQuadrant(current);
            int targetQuad = getQuadrant(target);

            switch (currentQuad) {
                case 1:
                    switch (targetQuad) {
                        case 1:
                            direction = 0;
                            break;
                        case 2:
                            direction = 1;
                            break;
                        case 3:
                            direction = 1;
                            break;
                        case 4:
                            direction = -1;
                            break;
                    }
                    break;
                case 2:
                    switch (targetQuad) {
                        case 1:
                            direction = -1;
                            break;
                        case 2:
                            direction = 0;
                            break;
                        case 3:
                            direction = 1;
                            break;
                        case 4:
                            direction = -1;
                            break;
                    }
                    break;
                case 3:
                    switch (targetQuad) {
                        case 1:
                            direction = -1;
                            break;
                        case 2:
                            direction = -1;
                            break;
                        case 3:
                            direction = 0;
                            break;
                        case 4:
                            direction = 1;
                            break;
                    }
                    break;
                case 4:
                    switch (targetQuad) {
                        case 1:
                            direction = 1;
                            break;
                        case 2:
                            direction = 1;
                            break;
                        case 3:
                            direction = -1;
                            break;
                        case 4:
                            direction = 0;
                            break;
                    }
                    break;
            }
        }


        switch (direction) {
            case 1: // go in a positive direction
                if(error > 0) {
                    setpoint = current + error;
                } else if(error < 0) {
                    setpoint = current + 360 + error;
                }
                break;
            case -1: // go in a negative direction
                if(error > 0) {
                    setpoint = current - 360 + error;
                } else if(error < 0) {
                    setpoint = current + error;
                }
                break;
            case 0: // go in the shortest path
                if(Math.abs(error) < 180) {
                    setpoint = current + error;
                } else if(error > 0){
                    setpoint = current - (360 - error);
                } else {
                    setpoint = current + error + 360;
                }
                break;
        }

        setPivotAngleTarget(setpoint);
    }
	
	public double getPropulsionVelocity() {
		return mPropulsion.getSelectedSensorVelocity(0);
	}
	
	public double getPropulsionPosition() {
		return mPropulsion.getSelectedSensorPosition(0);
	}
	
	public double getPivotAngle() {
		return (mPivot.getSelectedSensorPosition(0) - pivotOffset) / Constants.WHEEL_POD_PIVOT_TICKS_PER_DEG * pivotDirection;
	}

	public int getQuadrant(double angle) {
	    angle = angle % 360;

	    if((angle >= 0 && angle <= 90) || (angle >= -360 && angle <= -270)) {
	        return 1;
        }

        if((angle > 90 && angle <= 180) || (angle > -270 && angle <= -180)) {
            return 2;
        }

        if((angle > 180 && angle <= 270) || (angle > -180 && angle <= -90)) {
            return 3;
        }

        if((angle > 270 && angle < 360) || (angle > -90 && angle < 0)) {
            return 4;
        }

        Robot.log.sub("couldn't determine quadrant. angle: " + angle);
        return -1;

    }

	public double getPivotAngleAbsolute() {
	    double pos = getPivotAngle() % 360;

	    if(pos < 0) {
	        pos = 360 + pos;
        }

	    return pos;
    }

	public double getPivotAngleTarget() {
		return (mPivot.getClosedLoopTarget(0) - pivotOffset) / Constants.WHEEL_POD_PIVOT_TICKS_PER_DEG * pivotDirection;
	}

	public double getPropulsionVelocityTarget() {
	    return mPropulsion.getClosedLoopTarget(0);
    }

	public double getPivotAngleTargetAbsolute() {
	    return getPivotAngleTarget() % 360;
    }
	
	public double getPivotSpeed() {
		return mPivot.getSelectedSensorVelocity(0);
	}

	public void invertPropulsion(boolean invert) {
	    mPropulsion.setInverted(invert);
    }

    public void invertPivotAngle(boolean invert) {
	    pivotDirection = invert ? -1 : 1;
    }


    public boolean isInMecanum() {
	    return (getPivotAngleAbsolute() > 0 && getPivotAngleAbsolute() < 180) || (getPivotAngleAbsolute() < -180 && getPivotAngleAbsolute() > -360);
    }

    public boolean isPivotOnTarget() {
	    return Math.abs(getPivotAngleTarget() - getPivotAngle()) < Constants.PIVOT_TOLERANCE;
    }

	public String getName() {
		return name;
	}

	public void cleanup() {
        mPivot.set(ControlMode.Position, mPivot.getSelectedSensorPosition(0));

        mPropulsion.set(ControlMode.PercentOutput, 0.0);

    }

	public void updateSmartDashboard() {
        SmartDashboard.putNumber(name + " Pivot Angle", this.getPivotAngle());
        SmartDashboard.putNumber(name + " Pivot Target", this.getPivotAngleTarget());
        SmartDashboard.putNumber(name + " Pivot Abs", this.getPivotAngleAbsolute());
        SmartDashboard.putNumber(name + " Pivot Abs Tar", this.getPivotAngleTargetAbsolute());
        SmartDashboard.putNumber(name + " Pivot Raw", mPivot.getSelectedSensorPosition(0));

        SmartDashboard.putNumber(name + " Prop Vel", this.getPropulsionVelocity());
        SmartDashboard.putNumber(name + " Prop Pos", this.getPropulsionPosition());


	}
	
}
