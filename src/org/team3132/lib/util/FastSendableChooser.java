package org.team3132.lib.util;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;

public class FastSendableChooser<E> extends SendableChooser<E> {

	E currentSelection = null;
	
	public void update() {
		currentSelection = this.getSelected();
	}
	
	public E getFastSelected() {
		return currentSelection;
	}

}
