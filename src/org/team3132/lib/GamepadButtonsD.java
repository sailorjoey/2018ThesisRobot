package org.team3132.lib;

/**
 * Joystick gamepad button mapping.
 * This makes recognizing the buttons used easier.
 */
public class GamepadButtonsD {

    // GamepadButtons X - Y
    public static final int X_BUTTON = 1;
    public static final int A_BUTTON = 2;
    public static final int B_BUTTON = 3;
    public static final int Y_BUTTON = 4;

    // Bumpers and Triggers
    public static final int LEFT_BUMPER = 5;
    public static final int RIGHT_BUMPER = 6;
    public static final int LEFT_TRIGGER = 7;
    public static final int RIGHT_TRIGGER = 8;

    // Back and Start
    public static final int BACK_BUTTON = 9;
    public static final int START_BUTTON = 10;

    // Thumbstick press
    public static final int LEFT_THUMBSTICK_CLICK = 11;
    public static final int RIGHT_THUMBSTICK_CLICK = 12;
    
    //Axis
    public static final int LEFT_X_AXIS = 0;
    public static final int LEFT_Y_AXIS = 1;
    public static final int RIGHT_X_AXIS = 2;
    public static final int RIGHT_Y_AXIS = 3;
}
