package org.team3132.subsystems;

import edu.wpi.first.wpilibj.BuiltInAccelerometer;
import edu.wpi.first.wpilibj.interfaces.Accelerometer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.team3132.Constants;
import org.team3132.Robot;
import org.team3132.lib.Subsystem;

public class Drivebase extends Subsystem {

	private static Drivebase ourInstance = new Drivebase();
	
	public static Drivebase getInstance() {
		return ourInstance;
	}
	
	
	WheelPod pFrontLeft;
	WheelPod pFrontRight;
	WheelPod pBackLeft;
	WheelPod pBackRight;

	BuiltInAccelerometer accel;

	String sensorData = "";
	
	private Drivebase() {
		super("drivebase");

		pFrontLeft = new WheelPod("front left pod", Constants.DRIVE_LEFT_FRONT_PROPULSION_CAN_ID,
				Constants.DRIVE_LEFT_FRONT_PIVOT_CAN_ID, Constants.DRIVE_LEFT_FRONT_0_POS,
				Constants.DRIVE_LF_PROP_PHASE, Constants.DRIVE_LF_PIVOT_PHASE);
		pFrontRight = new WheelPod("front right pod", Constants.DRIVE_RIGHT_FRONT_PROPULSION_CAN_ID,
				Constants.DRIVE_RIGHT_FRONT_PIVOT_CAN_ID, Constants.DRIVE_RIGHT_FRONT_0_POS,
				Constants.DRIVE_RF_PROP_PHASE, Constants.DRIVE_RF_PIVOT_PHASE);
		pBackLeft = new WheelPod("back left pod", Constants.DRIVE_LEFT_BACK_PROPULSION_CAN_ID,
				Constants.DRIVE_LEFT_BACK_PIVOT_CAN_ID, Constants.DRIVE_LEFT_BACK_0_POS,
				Constants.DRIVE_LB_PROP_PHASE, Constants.DRIVE_LB_PIVOT_PHASE);
		pBackRight = new WheelPod("back right pod", Constants.DRIVE_RIGHT_BACK_PROPULSION_CAN_ID,
				Constants.DRIVE_RIGHT_BACK_PIVOT_CAN_ID, Constants.DRIVE_RIGHT_BACK_0_POS,
				Constants.DRIVE_RB_PROP_PHASE, Constants.DRIVE_RB_PIVOT_PHASE);

		pFrontLeft.invertPropulsion(true);
		pBackLeft.invertPropulsion(true);

		pFrontRight.invertPivotAngle(true);
		//pBackLeft.invertPivotAngle(false);
		pBackRight.invertPivotAngle(true);

		accel = new BuiltInAccelerometer();

        Robot.log.register(true, accel::getX, name + "/accel/X")
                .register(true, accel::getY, name + "/accel/Y")
                .register(true, accel::getZ, name + "/accel/Z");


	}

	public String getSensorData() {
		sensorData = "LF: Piv = " + pFrontLeft.getPivotAngle() + " ProV = " + pFrontLeft.getPropulsionVelocity() + " ProP = " + pFrontLeft.getPropulsionPosition()
				+ "   RF: Piv = " + pFrontRight.getPivotAngle() + " ProV = " + pFrontRight.getPropulsionVelocity() + " ProP = " + pFrontRight.getPropulsionPosition()
				+ "   LB: Piv = " + pBackLeft.getPivotAngle() + " ProV = " + pBackLeft.getPropulsionVelocity() + " ProP = " + pBackLeft.getPropulsionPosition()
				+ "   RB: Piv = " + pBackRight.getPivotAngle() + " ProV = " + pBackRight.getPropulsionVelocity() + " ProP = " + pBackRight.getPropulsionPosition();
		return sensorData;
	}

	public Accelerometer getAccel() {
	    return accel;
    }

	public boolean isInMecanumMode() {
	    return pFrontLeft.isInMecanum() && pBackLeft.isInMecanum() && pFrontRight.isInMecanum() && pBackRight.isInMecanum();
    }

    public void setPropulsions(double FL, double FR, double BL, double BR) {
	    pFrontLeft.setPropulsionSpeed(FL);
	    pFrontRight.setPropulsionSpeed(FR);
	    pBackLeft.setPropulsionSpeed(BL);
	    pBackRight.setPropulsionSpeed(BR);
    }

    public void setPivots(double FL, double FR, double BL, double BR){
        setPivots(FL, 0, FR, 0, BL, 0, BR, 0);
    }

    public void setPivots(double FL, int FLdir, double FR, int FRdir, double BL, int BLdir, double BR, int BRdir){
	    pFrontLeft.setPivotAngleTargetAbsolute(FL, FLdir);
	    pFrontRight.setPivotAngleTargetAbsolute(FR, FRdir);
	    pBackLeft.setPivotAngleTargetAbsolute(BL, BLdir);
	    pBackRight.setPivotAngleTargetAbsolute(BR, BRdir);
    }

    public void setFrontPivots(double front, int direction) {
        setPivots(front, direction, front, direction,
                pBackLeft.getPivotAngleTargetAbsolute(), 0, pBackRight.getPivotAngleTargetAbsolute(), 0);
    }

    public void setBackPivots(double back, int direction) {
        setPivots(pFrontLeft.getPivotAngleTargetAbsolute(), 0, pFrontRight.getPivotAngleTargetAbsolute(),
                0, back, direction, back, direction);
    }


    public void moveFrontPivotsIncrement(double increment) {
		pFrontLeft.setPivotAngleTarget(pFrontLeft.getPivotAngleTarget() + increment);
		pFrontRight.setPivotAngleTarget(pFrontRight.getPivotAngleTarget() + increment);
	}

	public void moveBackPivotsIncrement(double increment) {
		pBackLeft.setPivotAngleTarget(pBackLeft.getPivotAngleTarget() + increment);
		pBackRight.setPivotAngleTarget(pBackRight.getPivotAngleTarget() + increment);
	}
	
	public void moveBackLeftPivotIncrement(double increment) {
	    pBackLeft.setPivotAngleTarget(pBackLeft.getPivotAngleTarget() + increment);
    }
    
    public void moveBackRightPivotIncrement(double increment) {
        pBackRight.setPivotAngleTarget(pBackRight.getPivotAngleTarget() + increment);
    }

    public boolean isPivotOnTarget() {
	    return pFrontLeft.isPivotOnTarget() && pFrontRight.isPivotOnTarget()
                && pBackLeft.isPivotOnTarget() && pBackRight.isPivotOnTarget();
    }

	@Override
	public boolean initCheck() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean runningCheck() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cleanup() {
	    setAutomated(false);

		pFrontLeft.cleanup();
		pFrontRight.cleanup();
		pBackLeft.cleanup();
		pBackRight.cleanup();

	}


	@Override
	public void updateSmartDashboard() {
	    SmartDashboard.putBoolean("Drivebase mecanum mode", this.isInMecanumMode());

		SmartDashboard.putNumber("Accel X", accel.getX());
		SmartDashboard.putNumber("Accel Y", accel.getY());
		SmartDashboard.putNumber("Accel Z", accel.getZ());

		pFrontLeft.updateSmartDashboard();
		pFrontRight.updateSmartDashboard();
		pBackLeft.updateSmartDashboard();
		pBackRight.updateSmartDashboard();


	}

}
