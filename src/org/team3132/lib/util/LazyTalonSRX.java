package org.team3132.lib.util;

import com.ctre.phoenix.motorcontrol.ControlFrame;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

/**
 * This class is a thin wrapper around the CANTalon that reduces CAN bus / CPU overhead by skipping duplicate set
 * commands. (By default the Talon flushes the Tx buffer on every set call).
 */
public class LazyTalonSRX extends TalonSRX {
    protected double mLastSet = Double.NaN;
    protected ControlMode mLastControlMode = null;
    
    public LazyTalonSRX(int deviceNumber, int controlPeriodMs, int enablePeriodMs) {
        super(deviceNumber);
        this.setControlFramePeriod(controlPeriodMs, enablePeriodMs);
        this.clearStickyFaults(100);
    }

    public LazyTalonSRX(int deviceNumber, int controlPeriodMs) {
        super(deviceNumber);
        this.setControlFramePeriod(ControlFrame.Control_3_General, controlPeriodMs);
        this.clearStickyFaults(100);
    }

    public LazyTalonSRX(int deviceNumber) {
        super(deviceNumber);
        this.clearStickyFaults(100);
    }

    @Override
    public void set(ControlMode mode, double value) {
        if (value != mLastSet || mode != mLastControlMode) {
        	//System.out.println("set called on motor: " + this.getDeviceID() + "  mode: " + mode + "  val: " + value);
            mLastSet = value;
            mLastControlMode = getControlMode();
            super.set(mode, value);
        }
    }

    /*
    public String getDygraphHeader(String name) {
		String[] headers = {"baseID",
							"deviceID",
							"firwareVersion",
							"inverted",
							"hasResetOccurred",
							"busVoltage",
							"closedLoopTarget",
							"closedLoopError",
							"motorOutputPercent",
							"motorOutputVoltage",
							"outputCurrent",
							"integralAccumulator",
							"errorDerivative",
							"sensorPosition",
							"sensorVelocity",
							"getTemperature"};
		return DataLogUtil.formatDygraphHeader(name, headers);
	}
	
	public String getDygraphData() {
		int inverted = this.getInverted() ? 1:0;
		int reset = this.hasResetOccurred() ? 1:0;
		Number[] data = {this.getBaseID(),
						this.getDeviceID(),
						this.getFirmwareVersion(),
						inverted,
						reset,
						this.getBusVoltage(),
						this.getClosedLoopTarget(0),
						this.getClosedLoopError(0),
						this.getMotorOutputPercent(),
						this.getMotorOutputVoltage(),
						this.getOutputCurrent(),
						this.getIntegralAccumulator(0),
						this.getErrorDerivative(0),
						this.getSelectedSensorPosition(0),
						this.getSelectedSensorVelocity(0),
						this.getTemperature()};
		
		return DataLogUtil.formatDygraphData(data);
	}
	*/
}
