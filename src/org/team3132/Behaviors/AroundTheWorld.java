package org.team3132.Behaviors;

import org.team3132.lib.util.SystemUtil;

import static org.team3132.Behaviors.BasicStance.*;

public class AroundTheWorld extends AutomatedRoutine {

    public enum Direction{
        clockwise,
        antiClockwise
    };
    Direction direction = Direction.clockwise;

    public AroundTheWorld(Direction direction) {
        this.direction = direction;
    }

    @Override
    public void init() {
        //BasicStance.standUp();
    }

    @Override
    public void run() {
        BasicStance.waitForPivots();

        for(int i=0; i<2; i++) {
            tipForward();
            waitForPivots();

            if(direction == Direction.clockwise){
                tipRight();
            } else {
                tipLeft();
            }
            waitForPivots();

            tipBack();
            waitForPivots();

            if(direction == Direction.clockwise) {
                tipLeft();
            } else {
                tipRight();
            }
            waitForPivots();

        }

        standUp();
    }

    @Override
    public void kill() {

    }

    public void waitForPivots() {
        SystemUtil.delay(300);
    }
}
