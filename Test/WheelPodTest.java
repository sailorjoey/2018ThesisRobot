
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class WheelPodTest {

    double currentPivotAngle = 0;
    double targetAngle = 0;

    @Test
    public void testAbsolutePivot() {
        //System.out.println(-180%360);

        test(0,90,90,-270);
        test(0,180,180,-180);
        test(0,360,0,0);
        test(90,-90,270,-90);
        test(270,90,450,90);
        test(450,180, 540, 180);
        test(90,45,405,45);
        test(405,90,450,90);
        test(-180,180,-180,-180);
        test(-180,-270,90,-270);
        test(-270,90,-270,-270);
        test(-180,-90,-90,-450);
        test(180,-180,180,180);
        test(720,0,720,720);
        test(540, -45, 675, 315);
        test(1440, 0, 1440, 1440);
        test(0, 1440, 0, 0);

        currentPivotAngle = -270;
        assertEquals(-265.0, setPivotAngleTargetAbsolute(95, 0));

    }

    public void test(double current, double target, double forward, double reverse) {
        currentPivotAngle = current;
        assertEquals(forward, setPivotAngleTargetAbsolute(target, 1));
        assertEquals(reverse, setPivotAngleTargetAbsolute(target, -1));
    }

    public double setPivotAngleTargetAbsolute(double angle, int direction) {
        double current = getPivotAngleTarget();
        double target = angle % 360;
        double setpoint = current;
        double error = (target - getPivotAngleTargetAbsolute()) % 360;
        if(error%360 == 0) {
            error = 0;
        }

        System.out.println("current: " + current + " target: " + target + " setpoint: " + setpoint + " error: " + error);

        // chooses direction to go based on current and target quadrants
        if(direction == 0) {
            int currentQuad = getQuadrant(current);
            int targetQuad = getQuadrant(target);

            System.out.println("current quad: " + currentQuad);
            System.out.println("target quad: " + targetQuad);

            switch (currentQuad) {
                case 1:
                    switch (targetQuad) {
                        case 1:
                            direction = 0;
                            break;
                        case 2:
                            direction = 1;
                            break;
                        case 3:
                            direction = 1;
                            break;
                        case 4:
                            direction = -1;
                            break;
                    }
                    break;
                case 2:
                    switch (targetQuad) {
                        case 1:
                            direction = -1;
                            break;
                        case 2:
                            direction = 0;
                            break;
                        case 3:
                            direction = 1;
                            break;
                        case 4:
                            direction = -1;
                            break;
                    }
                    break;
                case 3:
                    switch (targetQuad) {
                        case 1:
                            direction = -1;
                            break;
                        case 2:
                            direction = -1;
                            break;
                        case 3:
                            direction = 0;
                            break;
                        case 4:
                            direction = 1;
                            break;
                    }
                    break;
                case 4:
                    switch (targetQuad) {
                        case 1:
                            direction = 1;
                            break;
                        case 2:
                            direction = 1;
                            break;
                        case 3:
                            direction = -1;
                            break;
                        case 4:
                            direction = 0;
                            break;
                    }
                    break;
            }
        }


        switch (direction) {
            case 1: // go in a positive direction
                if(error > 0) {
                    setpoint = current + error;
                } else if(error < 0) {
                    setpoint = current + 360 + error;
                }
                break;
            case -1: // go in a negative direction
                if(error > 0) {
                    setpoint = current - 360 + error;
                } else if(error < 0) {
                    setpoint = current + error;
                }
                break;
            case 0: // go in a logical path

                // shortest path
                if(Math.abs(error) < 180) {
                    setpoint = current + error;
                } else if(error > 0){
                    setpoint = current - (360 - error);
                } else {
                    setpoint = current + error + 360;
                }


                break;
        }

        return setpoint;
    }

    public double getPivotAngle() {
        return currentPivotAngle;
    }

    public double getPivotAngleAbsolute() {
        return getPivotAngle() % 360;
    }

    public double getPivotAngleTarget() {
        return currentPivotAngle;
    }

    public double getPivotAngleTargetAbsolute() {
        return getPivotAngleTarget() % 360;
    }

    public int getQuadrant(double angle) {
        angle = angle % 360;

        if((angle >= 0 && angle <= 90) || (angle >= -360 && angle <= -270)) {
            return 1;
        }

        if((angle > 90 && angle <= 180) || (angle > -270 && angle <= -180)) {
            return 2;
        }

        if((angle > 180 && angle <= 270) || (angle > -180 && angle <= -90)) {
            return 3;
        }

        if((angle > 270 && angle < 360) || (angle > -360 && angle <= -270)) {
            return 4;
        }

        System.out.println("couldn't determine quadrant. angle: " + angle);
        return -1;

    }


}
