package org.team3132.physicalTest;

import edu.wpi.first.wpilibj.Joystick;
import org.team3132.lib.GamepadButtonsX;
import org.team3132.lib.Test;
import org.team3132.subsystems.Drivebase;

public class FullPropulsionTest implements Test {


    Drivebase drivebase = Drivebase.getInstance();
    Joystick stick = new Joystick(0);

    int direction = 1;
    boolean inTest = false;
    int stateCounter = 0;
    final int stateMaxCount = 3;
    int cycleCounter = 0;
    final int cycleCountMax = 100;

    @Override
    public void testInit() {
        direction = 1;
        inTest = false;
        stateCounter = 0;
        cycleCounter = 0;
    }

    @Override
    public void testPeriodic() {
        if(inTest) {
            switch (stateCounter) {
                case 0:
                    drivebase.setPropulsions(direction, 0, 0, 0);
                    break;
                case 1:
                    drivebase.setPropulsions(0, direction, 0, 0);
                    break;
                case 2:
                    drivebase.setPropulsions(0, 0, direction, 0);
                    break;
                case 3:
                    drivebase.setPropulsions(0, 0, 0, direction);
                    break;
            }

            cycleCounter++;
            if(cycleCounter > cycleCountMax) {
                stateCounter++;
                cycleCounter = 0;
            }
            if(stateCounter > stateMaxCount) {
                stateCounter = 0;
                drivebase.setPropulsions(0,0,0,0);
                inTest = false;
            }

        } else {
            cycleCounter = 0;
            stateCounter = 0;

            if(stick.getRawButton(GamepadButtonsX.START_BUTTON)) {
                inTest = true;
                direction = 1;
                System.out.println(direction);
            } else if(stick.getRawButton(GamepadButtonsX.BACK_BUTTON)) {
                inTest = true;
                direction = -1;
                System.out.println(direction);
            }
        }
    }

    @Override
    public void testEnd() {
        drivebase.setPropulsions(0,0,0,0);
    }
}
