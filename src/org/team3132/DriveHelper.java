package org.team3132;

import org.team3132.lib.util.EventJoystick;
import org.team3132.subsystems.Drivebase;

public class DriveHelper {

    Drivebase drivebase = Drivebase.getInstance();


    public void smartDrive(EventJoystick stick) {

        if(drivebase.isAutomated()) {
            return;
        }

        double speedGain = 0.5;

        double throttle = -stick.getScaledAxis(1) * speedGain;
        double strafe = stick.getScaledAxis(0) * speedGain;
        double pivot = stick.getScaledAxis(2) * speedGain;


        double leftFront = 0;
        double leftBack = 0;
        double rightFront = 0;
        double rightBack = 0;


        switch(stick.getPOV()) {
            case 0:
                throttle = 0.3;
                strafe = 0;
                pivot = 0;
                break;
            case 90:
                throttle = 0;
                if(drivebase.isInMecanumMode()) {
                    strafe = 0.3;
                    pivot = 0;
                } else {
                    strafe = 0;
                    pivot = 0.3;
                }
                break;
            case 180:
                throttle = -0.3;
                strafe = 0;
                pivot = 0;
                break;
            case 270:
                throttle = 0;
                if(drivebase.isInMecanumMode()) {
                    strafe = -0.3;
                    pivot = 0;
                } else {
                    strafe = 0;
                    pivot = -0.3;
                }
                break;
            case 45:
                if(drivebase.isInMecanumMode()){
                    throttle = 0;
                    strafe = 0.3;
                    pivot = -0.2;
                } else {

                }
                break;
            case 135:
                if(drivebase.isInMecanumMode()){
                    throttle = 0;
                    strafe = 0.3;
                    pivot = 0.2;
                } else {

                }
                break;
            case 225:
                if(drivebase.isInMecanumMode()){
                    throttle = 0;
                    strafe = -0.3;
                    pivot = -0.3;
                } else {

                }
                break;
            case 315:
                if(drivebase.isInMecanumMode()){
                    throttle = 0;
                    strafe = -0.3;
                    pivot = 0.3;
                } else {

                }
                break;
        }

        if (!drivebase.isInMecanumMode()) {
          strafe = 0;
        }

        leftFront = throttle + pivot + strafe;
        leftBack = throttle + pivot - strafe;
        rightFront = throttle - pivot - strafe;
        rightBack = throttle - pivot + strafe;

        if(!drivebase.isAutomated()) {
            drivebase.setPropulsions(leftFront, rightFront, leftBack, rightBack);
        }

    }
}
