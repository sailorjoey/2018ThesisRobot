package org.team3132.lib.util;

import com.ctre.phoenix.ParamEnum;
import com.ctre.phoenix.motorcontrol.*;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import org.team3132.Robot;

public class TalonSRXFactory {

	public static class Configuration {
		public int CAN_MESSAGE_TIMEOUT_MS = 10;
		
		public int CONTROL_FRAME_PEROID_MS = 5;
		public int MOTION_CONTROL_FRAME_PERIOD_MS = 100;
		public int GENERAL_STATUS_FRAME_RATE_MS = 10;
		public int FEEDBACK_STATUS_FRAME_RATE_MS = 20;
		public int QUAD_ENCODER_STATUS_FRAME_RATE_MS = 160;
		public int ANALOG_TEMP_VBAT_STATUS_FRAME_RATE_MS = 160;
		public int PULSE_WIDTH_STATUS_FRAME_RATE_MS = 160;
		public int MOTION_TARGET_STATUS_FRAME_RATE_MS = 160;
		public int PIDF0_STATUS_FRAME_RATE_MS = 160;
		
		public int CLOSED_LOOP_CONTROL_PERIOD_MS = 1;
		public int MOTION_PROFILE_TRAJECTORY_PERIOD_MS = 1;
		
		public boolean INVERTED = false;
		public NeutralMode NEUTRAL_MODE = NeutralMode.Brake;
		public boolean SENSOR_PHASE = false;
		public double NEUTRAL_DEADBAND = 0.04;
		public double NOMINAL_OUTPUT_FORWARD = 0.0;
		public double NOMINAL_OUTPUT_REVERSE = 0.0;
		public double PEAK_OUTPUT_FORWARD = 1.0;
		public double PEAK_OUTPUT_REVERSE = -1.0;
		public double OPEN_LOOP_RAMP_SECONDS = 0.0;
		public double CLOSED_LOOP_RAMP_SECONDS = 0.0;
		
		public boolean ENABLE_VOLTAGE_COMPENSATION = false;
		public double VOLTAGE_COMPENSATION_SATURATION = 0;
		public int VOLTAGE_MEASUREMENT_FILTER_SAMPLES = 32;
		
		public VelocityMeasPeriod VELOCITY_MEASUREMENT_PERIOD = VelocityMeasPeriod.Period_100Ms;
		public int VELOCITY_MEASUREMENT_ROLLING_AVERAGE_WINDOW = 64;
		
		public boolean ENABLE_HEADING_HOLD = false;
		
		public double FEEDBACK_COEFFICIENT = 1.0;
		public double KP = 0.0;
		public double KI = 0.0;
		public double KD = 0.0;
		public double KF = 0.0;
		public int INTEGRAL_ZONE = 0;
		public double MAX_INTEGRAL_ACCUM = 1000.0;
		public int ALLOWABLE_CLOSED_LOOP_ERROR = 0;
		
		public boolean AUX_PID_POLARITY = false;
		public double CLOSED_LOOP_PEAK_OUTPUT = 1.0;
		
		public LimitSwitchSource FORWARD_LIMIT_SWITCH_SOURCE = LimitSwitchSource.Deactivated;
		public LimitSwitchNormal FORWARD_LIMIT_SWITCH_NORMAL = LimitSwitchNormal.NormallyOpen;
		public LimitSwitchSource REVERSE_LIMIT_SWITCH_SOURCE = LimitSwitchSource.Deactivated;
		public LimitSwitchNormal REVERSE_LIMIT_SWITCH_NORMAL = LimitSwitchNormal.NormallyOpen;
		
		public boolean FORWARD_SOFT_LIMIT_ENABLE = false;
		public int FORWARD_SENSOR_LIMIT = 0;
		public boolean REVERSE_SOFT_LIMIT_ENABLE = false;
		public int REVERSE_SENSOR_LIMIT = 0;
				
		public boolean ENABLE_CURRENT_LIMIT = false;
		public int CONTINUOUS_CURRENT_LIMIT = 0;
		public int PEAK_CURRENT_LIMIT_AMPS = 0;
		public int PEAK_CURRENT_DURATION_MS = 0;
		
		public int CLEAR_ON_FWD_LIMIT = 0;
		public int CLEAR_ON_REV_LIMIT = 0;
		public int CLEAR_ON_QUAD_IDX = 0;
		
	}
	
	private static final Configuration kFactoryConfig = new Configuration();
	private static final Configuration kDefaultConfig = new Configuration();
	private static final Configuration kSlaveConfig = new Configuration();
	private static final Configuration kLoggingConfig = new Configuration();
	
	static {
		// changes from factory defaults to preferred update rates
		kDefaultConfig.CONTROL_FRAME_PEROID_MS = 5;
		kDefaultConfig.MOTION_CONTROL_FRAME_PERIOD_MS = 100;
		kDefaultConfig.GENERAL_STATUS_FRAME_RATE_MS = 5;
		kDefaultConfig.FEEDBACK_STATUS_FRAME_RATE_MS = 100;
		kDefaultConfig.QUAD_ENCODER_STATUS_FRAME_RATE_MS = 100;
		kDefaultConfig.ANALOG_TEMP_VBAT_STATUS_FRAME_RATE_MS = 100;
		kDefaultConfig.PULSE_WIDTH_STATUS_FRAME_RATE_MS = 100;
		kDefaultConfig.MOTION_CONTROL_FRAME_PERIOD_MS = 100;
		kDefaultConfig.PIDF0_STATUS_FRAME_RATE_MS = 100;
		kDefaultConfig.ENABLE_VOLTAGE_COMPENSATION = true;
		
		// slow down feedback on slaves as they are not usually checked
		kSlaveConfig.CONTROL_FRAME_PEROID_MS = 1000;
		kSlaveConfig.MOTION_CONTROL_FRAME_PERIOD_MS = 1000;
		kSlaveConfig.GENERAL_STATUS_FRAME_RATE_MS = 1000;
		kSlaveConfig.FEEDBACK_STATUS_FRAME_RATE_MS = 1000;
		kSlaveConfig.QUAD_ENCODER_STATUS_FRAME_RATE_MS = 1000;
		kSlaveConfig.ANALOG_TEMP_VBAT_STATUS_FRAME_RATE_MS = 1000;
		kSlaveConfig.PULSE_WIDTH_STATUS_FRAME_RATE_MS = 1000;
		kSlaveConfig.MOTION_TARGET_STATUS_FRAME_RATE_MS = 1000;
		kSlaveConfig.PIDF0_STATUS_FRAME_RATE_MS = 1000;
		kSlaveConfig.ENABLE_VOLTAGE_COMPENSATION = true;

        kLoggingConfig.CONTROL_FRAME_PEROID_MS = 5;
        kLoggingConfig.MOTION_CONTROL_FRAME_PERIOD_MS = 100;
        kLoggingConfig.GENERAL_STATUS_FRAME_RATE_MS = 20;
        kLoggingConfig.FEEDBACK_STATUS_FRAME_RATE_MS = 20;
        kLoggingConfig.QUAD_ENCODER_STATUS_FRAME_RATE_MS = 20;
        kLoggingConfig.ANALOG_TEMP_VBAT_STATUS_FRAME_RATE_MS = 20;
        kLoggingConfig.PULSE_WIDTH_STATUS_FRAME_RATE_MS = 20;
        kLoggingConfig.MOTION_CONTROL_FRAME_PERIOD_MS = 50;
        kLoggingConfig.PIDF0_STATUS_FRAME_RATE_MS = 20;
        kLoggingConfig.ENABLE_VOLTAGE_COMPENSATION = true;
	}
	
	// Create a TalonSRX with the default configuration.
    public static TalonSRX createDefaultTalon(int id) {
        return createTalon(id, kDefaultConfig);
        //return createTalon(id, kLoggingConfig);
    }

    public static TalonSRX createPermanentSlaveTalon(int id, int master_id) {
        final TalonSRX talon = createTalon(id, kSlaveConfig);
        talon.set(ControlMode.Follower, master_id);

        return talon;
    }
    
    // Create a TalonSRX with the factory settings
    public static TalonSRX createFactoryTalon(int id) {
    	return createTalon(id, kFactoryConfig);
    }
    
    public static TalonSRX createTalon(int id, Configuration config) {
    	TalonSRX talon = new LazyTalonSRX(id, config.CONTROL_FRAME_PEROID_MS);
    	talon.set(ControlMode.PercentOutput, 0.0);
        
    	talon.setControlFramePeriod(ControlFrame.Control_3_General, config.CONTROL_FRAME_PEROID_MS);
    	//talon.setControlFramePeriod(ControlFrame.Control_4_Advanced, config.CONTROL_FRAME_PEROID_MS);
    	//talon.setControlFramePeriod(ControlFrame.Control_6_MotProfAddTrajPoint, config.CONTROL_FRAME_PEROID_MS);
    	talon.setStatusFramePeriod(StatusFrameEnhanced.Status_1_General, config.GENERAL_STATUS_FRAME_RATE_MS, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, config.FEEDBACK_STATUS_FRAME_RATE_MS, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.setStatusFramePeriod(StatusFrameEnhanced.Status_3_Quadrature, config.QUAD_ENCODER_STATUS_FRAME_RATE_MS, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.setStatusFramePeriod(StatusFrameEnhanced.Status_4_AinTempVbat, config.ANALOG_TEMP_VBAT_STATUS_FRAME_RATE_MS, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.setStatusFramePeriod(StatusFrameEnhanced.Status_8_PulseWidth, config.PULSE_WIDTH_STATUS_FRAME_RATE_MS, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.setStatusFramePeriod(StatusFrameEnhanced.Status_10_MotionMagic, config.MOTION_TARGET_STATUS_FRAME_RATE_MS, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.setStatusFramePeriod(StatusFrameEnhanced.Status_13_Base_PIDF0, config.PIDF0_STATUS_FRAME_RATE_MS, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.setStatusFramePeriod(StatusFrameEnhanced.Status_14_Turn_PIDF1, config.PIDF0_STATUS_FRAME_RATE_MS, config.CAN_MESSAGE_TIMEOUT_MS);
    	
    	talon.configClosedLoopPeriod(0, config.CLOSED_LOOP_CONTROL_PERIOD_MS, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configClosedLoopPeriod(1, config.CLOSED_LOOP_CONTROL_PERIOD_MS, config.CAN_MESSAGE_TIMEOUT_MS);
    	
    	talon.setInverted(config.INVERTED);
    	talon.setNeutralMode(config.NEUTRAL_MODE);
    	talon.setSensorPhase(config.SENSOR_PHASE);
    	talon.configNeutralDeadband(config.NEUTRAL_DEADBAND, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configNominalOutputForward(config.NOMINAL_OUTPUT_FORWARD, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configNominalOutputReverse(config.NOMINAL_OUTPUT_REVERSE, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configPeakOutputForward(config.PEAK_OUTPUT_FORWARD, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configPeakOutputReverse(config.PEAK_OUTPUT_REVERSE, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configOpenloopRamp(config.OPEN_LOOP_RAMP_SECONDS, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configClosedloopRamp(config.CLOSED_LOOP_RAMP_SECONDS, config.CAN_MESSAGE_TIMEOUT_MS);
    	
    	talon.enableVoltageCompensation(config.ENABLE_VOLTAGE_COMPENSATION);
    	talon.configVoltageCompSaturation(config.VOLTAGE_COMPENSATION_SATURATION, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configVoltageMeasurementFilter(config.VOLTAGE_MEASUREMENT_FILTER_SAMPLES, config.CAN_MESSAGE_TIMEOUT_MS);
    	
    	talon.configVelocityMeasurementPeriod(config.VELOCITY_MEASUREMENT_PERIOD, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configVelocityMeasurementWindow(config.VELOCITY_MEASUREMENT_ROLLING_AVERAGE_WINDOW, config.CAN_MESSAGE_TIMEOUT_MS);
    	
    	talon.enableHeadingHold(config.ENABLE_HEADING_HOLD);
    	talon.configAuxPIDPolarity(config.AUX_PID_POLARITY, config.CAN_MESSAGE_TIMEOUT_MS);
    	
    	talon.configSelectedFeedbackCoefficient(config.FEEDBACK_COEFFICIENT, 0, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.config_kP(0, config.KP, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.config_kI(0, config.KI, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.config_kD(0, config.KD, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.config_kF(0, config.KF, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.config_IntegralZone(0, config.INTEGRAL_ZONE, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configMaxIntegralAccumulator(0, config.MAX_INTEGRAL_ACCUM, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configAllowableClosedloopError(0, config.ALLOWABLE_CLOSED_LOOP_ERROR, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configClosedLoopPeakOutput(0, config.CLOSED_LOOP_PEAK_OUTPUT, config.CAN_MESSAGE_TIMEOUT_MS);
    	
    	talon.configSelectedFeedbackCoefficient(config.FEEDBACK_COEFFICIENT, 1, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.config_kP(1, config.KP, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.config_kI(1, config.KI, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.config_kD(1, config.KD, config.CAN_MESSAGE_TIMEOUT_MS);
		talon.config_kF(1, config.KF, config.CAN_MESSAGE_TIMEOUT_MS);
		talon.config_IntegralZone(1, config.INTEGRAL_ZONE, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configMaxIntegralAccumulator(1, config.MAX_INTEGRAL_ACCUM, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configAllowableClosedloopError(1, config.ALLOWABLE_CLOSED_LOOP_ERROR, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configClosedLoopPeakOutput(1, config.CLOSED_LOOP_PEAK_OUTPUT, config.CAN_MESSAGE_TIMEOUT_MS);
    	
    	talon.configForwardLimitSwitchSource(config.FORWARD_LIMIT_SWITCH_SOURCE, config.FORWARD_LIMIT_SWITCH_NORMAL, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configReverseLimitSwitchSource(config.REVERSE_LIMIT_SWITCH_SOURCE, config.REVERSE_LIMIT_SWITCH_NORMAL, config.CAN_MESSAGE_TIMEOUT_MS);
    	
    	talon.configForwardSoftLimitEnable(config.FORWARD_SOFT_LIMIT_ENABLE, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configForwardSoftLimitThreshold(config.FORWARD_SENSOR_LIMIT, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configReverseSoftLimitEnable(config.REVERSE_SOFT_LIMIT_ENABLE, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configReverseSoftLimitThreshold(config.REVERSE_SENSOR_LIMIT, config.CAN_MESSAGE_TIMEOUT_MS);
    	
    	talon.enableCurrentLimit(config.ENABLE_CURRENT_LIMIT);
    	talon.configContinuousCurrentLimit(config.CONTINUOUS_CURRENT_LIMIT, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configPeakCurrentLimit(config.PEAK_CURRENT_LIMIT_AMPS, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configPeakCurrentDuration(config.PEAK_CURRENT_DURATION_MS, config.CAN_MESSAGE_TIMEOUT_MS);
    	
    	talon.configSetParameter(ParamEnum.eClearPositionOnQuadIdx, config.CLEAR_ON_QUAD_IDX, 0x00, 0x00, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configSetParameter(ParamEnum.eClearPositionOnLimitF, config.CLEAR_ON_FWD_LIMIT, 0x00, 0x00, config.CAN_MESSAGE_TIMEOUT_MS);
    	talon.configSetParameter(ParamEnum.eClearPositionOnLimitR, config.CLEAR_ON_REV_LIMIT, 0x00, 0x00, config.CAN_MESSAGE_TIMEOUT_MS);


    	if(Robot.log != null) {
            //Robot.log.register(false, talon::getBaseID,"talon/" + id + "/baseID");
            //Robot.log.register(false,talon::getDeviceID,"talon/" + id + "/deviceID");
            //Robot.log.register(false,talon::getFirmwareVersion,"talon/" + id + "/firmwareVersion");
            Robot.log.register(false,() -> talon.getInverted()?1:0,"talon/" + id + "/inverted");
            Robot.log.register(false,() -> talon.hasResetOccurred()?1:0,"talon/" + id + "/hasResetOccurred");
            Robot.log.register(false,talon::getBusVoltage,"talon/" + id + "/busVoltage");
            Robot.log.register(false,() -> talon.getClosedLoopTarget(0),"talon/" + id + "/closedLoopTarget");
            Robot.log.register(false,() -> talon.getClosedLoopError(0),"talon/" + id + "/closedLoopError");
            Robot.log.register(false, talon::getMotorOutputPercent,"talon/" + id + "/motorOutputPercent");
            Robot.log.register(false, talon::getMotorOutputVoltage,"talon/" + id + "/motorOutputVoltage");
            Robot.log.register(false, talon::getOutputCurrent,"talon/" + id + "/outputCurrent");
            Robot.log.register(false,() -> talon.getIntegralAccumulator(0),"talon/" + id + "/integralAccumulator");
            Robot.log.register(false,() -> talon.getErrorDerivative(0),"talon/" + id + "/errorDerivative");
            Robot.log.register(false,() -> talon.getSelectedSensorPosition(0),"talon/" + id + "/sensorPosition");
            Robot.log.register(false,() -> talon.getSelectedSensorVelocity(0),"talon/" + id + "/sensorVelocity");
            Robot.log.register(false, talon::getTemperature,"talon/" + id + "/temperature");

            Robot.log.sub("!!!!!!!!!ROBOT LOG TALON " + id +" REGISTERED!!!!!!!!!!!");
        } else {
            Robot.log.sub("\n \n \n !!!!!!!!!ROBOT LOG TALON " + id + " REGISTERED!!!!!!!!!!!\n \n \n ");
        }


    	return talon;
    }
    
    
    /*
     * run this on a new talon to get default parameters
     */
    public static String getFullTalonInfo(TalonSRX talon) {
    	StringBuilder sb = new StringBuilder().append("getBusVoltage = ").append(talon.getBusVoltage())
				.append("\n").append("getClass = ").append(talon.getClass())
				.append("\n").append("getDeviceID = ").append(talon.getDeviceID())
				.append("\n").append("GetFirmwareVersion = ").append(talon.getFirmwareVersion())
				.append("\n").append("toString = ").append(talon.toString())
				.append("\n").append("getTemperature = ").append(talon.getTemperature())
				.append("\n")

				.append("\n").append("getStatusFramePeriod: 1_General = ").append(talon.getStatusFramePeriod(StatusFrameEnhanced.Status_1_General,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("getStatusFramePeriod: 2_Feedback0 = ").append(talon.getStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("getStatusFramePeriod: 3_Quadrature = ").append(talon.getStatusFramePeriod(StatusFrameEnhanced.Status_3_Quadrature,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("getStatusFramePeriod: 4_AinTempVbat = ").append(talon.getStatusFramePeriod(StatusFrame.Status_4_AinTempVbat,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("getStatusFramePeriod: 6_Misc = ").append(talon.getStatusFramePeriod(StatusFrame.Status_6_Misc,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("getStatusFramePeriod: 7_CommStatus = ").append(talon.getStatusFramePeriod(StatusFrame.Status_7_CommStatus,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("getStatusFramePeriod: 8_PulseWidth = ").append(talon.getStatusFramePeriod(StatusFrameEnhanced.Status_8_PulseWidth,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("getStatusFramePeriod: 9_MotProfBuffer = ").append(talon.getStatusFramePeriod(StatusFrameEnhanced.Status_9_MotProfBuffer,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("getStatusFramePeriod: 10_MotionMagic = ").append(talon.getStatusFramePeriod(StatusFrame.Status_10_MotionMagic,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("getStatusFramePeriod: 12_Feedback1 = ").append(talon.getStatusFramePeriod(StatusFrameEnhanced.Status_12_Feedback1,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("getStatusFramePeriod: 13_Base_PIDF0 = ").append(talon.getStatusFramePeriod(StatusFrame.Status_13_Base_PIDF0,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("getStatusFramePeriod: 14_Turn_PIDF0 = ").append(talon.getStatusFramePeriod(StatusFrame.Status_14_Turn_PIDF1,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("ePIDLoopPeriod0 = ").append(talon.configGetParameter(ParamEnum.ePIDLoopPeriod,0,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("ePIDLoopPeriod1 = ").append(talon.configGetParameter(ParamEnum.ePIDLoopPeriod,1,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("getInverted = ").append(talon.getInverted())
				.append("\n").append("eOnBoot_BrakeMode = ").append(talon.configGetParameter(ParamEnum.eOnBoot_BrakeMode, 0, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eSensorTerm = ").append(talon.configGetParameter(ParamEnum.eSensorTerm, 0, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eNeutralDeadband = ").append(talon.configGetParameter(ParamEnum.eNeutralDeadband, 0, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eNominalPosOutput = ").append(talon.configGetParameter(ParamEnum.eNominalPosOutput, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eNominalNegOutput = ").append(talon.configGetParameter(ParamEnum.eNominalNegOutput, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("ePeakPosOutput = ").append(talon.configGetParameter(ParamEnum.ePeakPosOutput, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("ePeakNegOutput = ").append(talon.configGetParameter(ParamEnum.ePeakNegOutput, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eOpenLoopRamp = ").append(talon.configGetParameter(ParamEnum.eOpenloopRamp, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eClosedLoopRamp = ").append(talon.configGetParameter(ParamEnum.eClosedloopRamp, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("eNominalBatteryVoltage = ").append(talon.configGetParameter(ParamEnum.eNominalBatteryVoltage, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eBatteryVoltageFilterSize = ").append(talon.configGetParameter(ParamEnum.eBatteryVoltageFilterSize, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eTempCompDisable = ").append(talon.configGetParameter(ParamEnum.eTempCompDisable, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eFusedHeadingOffset = ").append(talon.configGetParameter(ParamEnum.eFusedHeadingOffset, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("GetVelocityMeasurementPeriod = ").append(talon.configGetParameter(ParamEnum.eSampleVelocityPeriod, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("GetVelocityMeasurementWindow = ").append(talon.configGetParameter(ParamEnum.eSampleVelocityWindow, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("eSelectedSensorCoefficient = ").append(talon.configGetParameter(ParamEnum.eSelectedSensorCoefficient, 0, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_P = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_P, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_I = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_I, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_D = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_D, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_F = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_F, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_IZone = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_IZone, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_MaxIAccum = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_MaxIAccum, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_AllowableErr = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_AllowableErr, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_PeakOutput = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_PeakOutput, 0, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("eSelectedSensorCoefficient = ").append(talon.configGetParameter(ParamEnum.eSelectedSensorCoefficient, 1, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_P = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_P, 1, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_I = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_I, 1, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_D = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_D, 1, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_F = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_F, 1, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_IZone = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_IZone, 1, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_MaxIAccum = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_MaxIAccum, 1, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_AllowableErr = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_AllowableErr, 1, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eProfileParamSlot_PeakOutput = ").append(talon.configGetParameter(ParamEnum.eProfileParamSlot_PeakOutput, 1, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("eLimitSwitchSource = ").append(talon.configGetParameter(ParamEnum.eLimitSwitchSource,0,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eRemoteSensorSource = ").append(talon.configGetParameter(ParamEnum.eRemoteSensorSource,0,kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("eForwardSoftLimitEnable = ").append(talon.configGetParameter(ParamEnum.eForwardSoftLimitEnable, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eForwardSoftLimitThreshold = ").append(talon.configGetParameter(ParamEnum.eForwardSoftLimitThreshold, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eReverseSoftLimitEnable = ").append(talon.configGetParameter(ParamEnum.eReverseSoftLimitEnable, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("eReverseSoftLimitThreshold = ").append(talon.configGetParameter(ParamEnum.eReverseSoftLimitThreshold, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("ePeakCurrentLimitAmps = ").append(talon.configGetParameter(ParamEnum.ePeakCurrentLimitAmps, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("ePeakCurrentLimitMs = ").append(talon.configGetParameter(ParamEnum.ePeakCurrentLimitMs, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("ePeakCurrentLimitAmps = ").append(talon.configGetParameter(ParamEnum.ePeakCurrentLimitAmps, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("isZeroSensorPosOnIndexEnabled = ").append(talon.configGetParameter(ParamEnum.eClearPositionOnIdx, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("isZeroSensorPosOnFwdLimitEnabled = ").append(talon.configGetParameter(ParamEnum.eClearPositionOnLimitF, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n").append("isZeroSensorPosOnRevLimitEnabled = ").append(talon.configGetParameter(ParamEnum.eClearPositionOnLimitR, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

                .append("\n").append("getMotionMagicActTrajPosition = ").append(talon.getActiveTrajectoryVelocity())
				.append("\n").append("getMotionMagicCruiseVelocity = ").append(talon.configGetParameter(ParamEnum.eMotMag_VelCruise, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("GetIaccum = ").append(talon.getIntegralAccumulator(0))
				.append("\n").append("getControlMode = ").append(talon.getControlMode())
				.append("\n").append("getMotionMagicAcceleration = ").append(talon.configGetParameter(ParamEnum.eMotMag_Accel, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
				.append("\n")

				.append("\n").append("eStickyFaults = ").append(talon.configGetParameter(ParamEnum.eStickyFaults, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
                .append("\n").append("getMotionProfileTopLevelBufferCount = ").append(talon.getMotionProfileTopLevelBufferCount())
                .append("\n").append("ePulseWidthPeriod_EdgesPerRot = ").append(talon.configGetParameter(ParamEnum.ePulseWidthPeriod_EdgesPerRot, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
                .append("\n").append("getClosedLoopError(0) = ").append(talon.getClosedLoopError(0))
				.append("\n").append("getOutputVoltage = ").append(talon.getMotorOutputVoltage())
                .append("\n").append("getPulseWidthPosition = ").append(talon.configGetParameter(ParamEnum.ePulseWidthPosition, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
                .append("\n").append("getOutputCurrent = ").append(talon.getOutputCurrent())
                .append("\n").append("getSelectedSensorPosition = ").append(talon.getSelectedSensorPosition(0))
                .append("\n").append("getSelectedSensorVelocity = ").append(talon.getSelectedSensorVelocity(0))
                .append("\n").append("getEncPosition = ").append(talon.configGetParameter(ParamEnum.eQuadraturePosition, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
                .append("\n").append("getAnalogInPosition = ").append(talon.configGetParameter(ParamEnum.eAnalogPosition, 0x00, kDefaultConfig.CAN_MESSAGE_TIMEOUT_MS))
                .append("\n").append("getLastError = ").append(talon.getLastError())
                .append("\n").append("getPinStateQuadIdx = ").append(talon.getSensorCollection().getPinStateQuadIdx())
                .append("\n").append("getAnalogInRaw = ").append(talon.getSensorCollection().getAnalogInRaw())
                .append("\n").append("getSelectedSensorVelocity = ").append(talon.getSelectedSensorVelocity(0))
                .append("\n").append("getClosedLoopError = ").append(talon.getClosedLoopError(0))
                .append("\n").append("getClosedLoopTarget = ").append(talon.getClosedLoopTarget(0))
                .append("\n").append("isMotionProfileTopLevelBufferFull = ").append(talon.isMotionProfileTopLevelBufferFull())
                .append("\n").append("hashCode = ").append(talon.hashCode())
                .append("\n").append("isFwdLimitSwitchClosed = ").append(talon.getSensorCollection().isFwdLimitSwitchClosed())
                .append("\n").append("getPinStateQuadA = ").append(talon.getSensorCollection().getPinStateQuadA())
                .append("\n").append("getPinStateQuadB = ").append(talon.getSensorCollection().getPinStateQuadB())
                .append("\n").append("getPulseWidthVelocity = ").append(talon.getSensorCollection().getPulseWidthVelocity())
                .append("\n").append("getSelectedSensorPosition = ").append(talon.getSelectedSensorPosition(0))
                .append("\n").append("getPulseWidthRiseToFallUs = ").append(talon.getSensorCollection().getPulseWidthRiseToFallUs());


    	return sb.toString();
    }
	
}
