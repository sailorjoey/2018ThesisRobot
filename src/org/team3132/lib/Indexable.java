package org.team3132.lib;

public interface Indexable {

	/**
	 * Checks if the system has been zeroed or indexed
	 * @return true if the system is zeroed
	 */
	public boolean isIndexed();
	
	/**
	 * this will be called while the robot is disabled
	 * Put code here to index the subsystem while the robot is disabled
	 */
	public void disabledIndexUpdate();
}
