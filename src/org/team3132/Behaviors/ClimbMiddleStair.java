package org.team3132.Behaviors;

import org.team3132.lib.util.SystemUtil;
import org.team3132.subsystems.Drivebase;

public class ClimbMiddleStair extends AutomatedRoutine {

    Drivebase drivebase = Drivebase.getInstance();

    @Override
    public void init() {
        drivebase.setAutomated(true);
    }

    @Override
    public void run() {
        waitForPivots();
        // turn on drive
        drivebase.setPropulsions(0.2, 0.2, 0.2, 0.2);

        // lift front up first stair
        drivebase.setFrontPivots(-30, -1);
        waitForPivots();

        // turn off drive
        drivebase.setPropulsions(0,0,0,0);

        // lift up back and reset front
        drivebase.setPivots(90, 1, 90, 1, -50, 1, -50, 1);
        waitForPivots();

        // turn on drive and lift front up second stair
        drivebase.setPropulsions(0.2, 0.2, 0.2, 0.2);
        drivebase.setFrontPivots(-30, -1);
        waitForPivots();

        // reset front
        drivebase.setFrontPivots(80, 1);
        waitForPivots();

        // turn on drive
        drivebase.setPropulsions(0.3, 0.3, 0.3, 0.3);
        SystemUtil.delay(200); // added because it was slipping down before the motors start

        // flip up back right leg
        //drivebase.setPivots(90, 0,90,0, -60, 0, -50, 1);
        //waitForPivots();
        drivebase.setPivots(90, 0,90,0, -60, 0, 200, 1);
        waitForPivots();

        // flip up back left leg
        drivebase.setPivots(90, 0, 90, 0, 200, 1, 200, 0);
        waitForPivots();

        // lift up on both back legs
        drivebase.setBackPivots(-100, 1);

        // turn off drive
        drivebase.setPropulsions(0, 0, 0, 0);

        //waitForPivots();
        drivebase.setAutomated(false);
    }

    @Override
    public void kill() {
        drivebase.cleanup();
    }

    public void waitForPivots() {
        while (!drivebase.isPivotOnTarget()) {
            SystemUtil.delay(50);
        }

        //SystemUtil.delay(500);
    }
}
