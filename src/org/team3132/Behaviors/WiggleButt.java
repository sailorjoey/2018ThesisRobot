package org.team3132.Behaviors;

import org.team3132.lib.util.SystemUtil;
import org.team3132.subsystems.Drivebase;

import static org.team3132.Behaviors.BasicStance.waitForPivots;

public class WiggleButt extends AutomatedRoutine {

    Drivebase drivebase = Drivebase.getInstance();
    double fl;
    double fr;
    double bl;
    double br;

    @Override
    public void init() {
        drivebase.setAutomated(true);

    }

    @Override
    public void run() {
        drivebase.setPivots(0,0,-60,-60);
        waitForPivots();

        for(int i=0; i<3; i++) {
            drivebase.setPivots(-5, 0, -65, -55);
            //waitForPivots();
            SystemUtil.delay(150);
            drivebase.setPivots(0,-5,-55,-65);
            //waitForPivots();
            SystemUtil.delay(150);
        }

        BasicStance.standUp();
        waitForPivots();
        drivebase.setAutomated(false);
    }

    @Override
    public void kill() {
        drivebase.setAutomated(false);
    }

}
