package org.team3132.lib.util;

/**
 * Tool for dealing with arrays of numbers
 */
public class NumberArrayUtil {
    /**
     * Sorts an int array
     * @param array Array to sort
     * @return Sorted array
     */
    public static int[] sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int e = i;
            while (e > 0 && array[e] < array[e - 1]) {
                int prev = array[e - 1];
                array[e - 1] = array[e];
                array[e] = prev;
                e--;
            }
        }
        return array;
    }

    /**
     * Sorts a double array
     * @param array Array to sort
     * @return Sorted array
     */
    public static double[] sort(double[] array) {
        for (int i = 1; i < array.length; i++) {
            int e = i;
            while (e > 0 && array[e] < array[e - 1]) {
                double prev = array[e - 1];
                array[e - 1] = array[e];
                array[e] = prev;
                e--;
            }
        }
        return array;
    }

    /**
     * Sorts a float array
     * @param array Array to sort
     * @return Sorted array
     */
    public static float[] sort(float[] array) {
        for (int i = 1; i < array.length; i++) {
            int e = i;
            while (e > 0 && array[e] < array[e - 1]) {
                float prev = array[e - 1];
                array[e - 1] = array[e];
                array[e] = prev;
                e--;
            }
        }
        return array;
    }

    /**
     * Add a number to all elements of an array
     * @param array Array to add to
     * @param amount Number to add
     * @return Array with items added
     */
    public static int[] addition(int[] array, int amount) {
        return addition(array, amount, 0, array.length);
    }

    /**
     * Add a number to all elements of an array
     * @param array Array to add to
     * @param amount Number to add
     * @return Array with items added
     */
    public static double[] addition(double[] array, double amount) {
        return addition(array, amount, 0, array.length);
    }

    /**
     * Add a number to all elements of an array
     * @param array Array to add to
     * @param amount Number to add
     * @return Array with items added
     */
    public static float[] addition(float[] array, float amount) {
        return addition(array, amount, 0, array.length);
    }

    /**
     * Add a number to the elements of an array from a certain point
     * @param array Array to add to
     * @param amount Number to add
     * @param startIndex Index to start from
     * @return Array with items added
     */
    public static int[] addition(int[] array, int amount, int startIndex) {
        return addition(array, amount, startIndex, array.length);
    }

    /**
     * Add a number to the elements of an array from a certain point
     * @param array Array to add to
     * @param amount Number to add
     * @param startIndex Index to start from
     * @return Array with items added
     */
    public static double[] addition(double[] array, double amount, int startIndex) {
        return addition(array, amount, startIndex, array.length);
    }

    /**
     * Add a number to the elements of an array from a certain point
     * @param array Array to add to
     * @param amount Number to add
     * @param startIndex Index to start from
     * @return Array with items added
     */
    public static float[] addition(float[] array, float amount, int startIndex) {
        return addition(array, amount, startIndex, array.length);
    }

    /**
     * Add a number to the specified range of the array
     * @param array Array to add to
     * @param amount Number to add
     * @param startIndex Index to start from
     * @param endIndex Index to end at
     * @return Array with items added
     */
    public static int[] addition(int[] array, int amount, int startIndex, int endIndex) {
        for (; startIndex < endIndex; startIndex++) array[startIndex] += amount;
        return array;
    }

    /**
     * Add a number to the specified range of the array
     * @param array Array to add to
     * @param amount Number to add
     * @param startIndex Index to start from
     * @param endIndex Index to end at
     * @return Array with items added
     */
    public static double[] addition(double[] array, double amount, int startIndex, int endIndex) {
        for (; startIndex < endIndex; startIndex++) array[startIndex] += amount;
        return array;
    }

    /**
     * Add a number to the specified range of the array
     * @param array Array to add to
     * @param amount Number to add
     * @param startIndex Index to start from
     * @param endIndex Index to end at
     * @return Array with items added
     */
    public static float[] addition(float[] array, float amount, int startIndex, int endIndex) {
        for (; startIndex < endIndex; startIndex++) array[startIndex] += amount;
        return array;
    }

    /**
     * Subtract a number from all elements of an array
     * @param array Array to subtract from
     * @param amount Number to subtract
     * @return Array with items subtracted
     */
    public static int[] subtract(int[] array, int amount) {
       return subtract(array, amount, 0, array.length);
    }

    /**
     * Subtract a number from all elements of an array
     * @param array Array to subtract from
     * @param amount Number to subtract
     * @return Array with items subtracted
     */
    public static double[] subtract(double[] array, double amount) {
        return subtract(array, amount, 0, array.length);
    }

    /**
     * Subtract a number from all elements of an array
     * @param array Array to subtract from
     * @param amount Number to subtract
     * @return Array with items subtracted
     */
    public static float[] subtract(float[] array, float amount) {
        return subtract(array, amount, 0, array.length);
    }

    /**
     * Subtract a number from a certain point onwards in an array
     * @param array Array to subtract from
     * @param amount Number to subtract
     * @param startIndex Index to start from
     * @return Array with items subtracted
     */
    public static int[] subtract(int[] array, int amount, int startIndex) {
        return subtract(array, amount, startIndex, array.length);
    }

    /**
     * Subtract a number from a certain point onwards in an array
     * @param array Array to subtract from
     * @param amount Number to subtract
     * @param startIndex Index to start from
     * @return Array with items subtracted
     */
    public static double[] subtract(double[] array, double amount, int startIndex) {
        return subtract(array, amount, startIndex, array.length);
    }

    /**
     * Subtract a number from a certain point onwards in an array
     * @param array Array to subtract from
     * @param amount Number to subtract
     * @param startIndex Index to start from
     * @return Array with items subtracted
     */
    public static float[] subtract(float[] array, float amount, int startIndex) {
        return subtract(array, amount, startIndex, array.length);
    }

    /**
     * Subtract a number from the specified range of the array
     * @param array Array to subtract from
     * @param amount Number to subtract
     * @param startIndex Index to start from
     * @param endIndex Index to end at
     * @return Array with items subtracted
     */
    public static int[] subtract(int[] array, int amount, int startIndex, int endIndex) {
        for (; startIndex < endIndex; startIndex++) array[startIndex] -= amount;
        return array;
    }

    /**
     * Subtract a number from the specified range of the array
     * @param array Array to subtract from
     * @param amount Number to subtract
     * @param startIndex Index to start from
     * @param endIndex Index to end at
     * @return Array with items subtracted
     */
    public static double[] subtract(double[] array, double amount, int startIndex, int endIndex) {
        for (; startIndex < endIndex; startIndex++) array[startIndex] -= amount;
        return array;
    }

    /**
     * Subtract a number from the specified range of the array
     * @param array Array to subtract from
     * @param amount Number to subtract
     * @param startIndex Index to start from
     * @param endIndex Index to end at
     * @return Array with items subtracted
     */
    public static float[] subtract(float[] array, float amount, int startIndex, int endIndex) {
        for (; startIndex < endIndex; startIndex++) array[startIndex] -= amount;
        return array;
    }

    /**
     * Checks if a certain number is in an array
     * @param array Array to check
     * @param num Number to check for
     * @return If number is in the array
     */
    public static boolean numInArray(int[] array, int num) {
        for (int item : array) {
            if (item == num) return true;
        }
        return false;
    }

    /**
     * Checks if a certain number is in an array
     * @param array Array to check
     * @param num Number to check for
     * @return If number is in the array
     */
    public static boolean numInArray(double[] array, double num) {
        for (double item : array) {
            if (item == num) return true;
        }
        return false;
    }

    /**
     * Checks if a certain number is in an array
     * @param array Array to check
     * @param num Number to check for
     * @return If number is in the array
     */
    public static boolean numInArray(float[] array, float num) {
        for (float item : array) {
            if (item == num) return true;
        }
        return false;
    }
}
