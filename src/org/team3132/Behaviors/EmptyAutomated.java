package org.team3132.Behaviors;

import org.team3132.lib.util.SystemUtil;

public class EmptyAutomated extends AutomatedRoutine {

    @Override
    public void init() {
        // TODO Auto-generated method stub

    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        System.out.println("Running empty routine");
        SystemUtil.delay(5000);

    }

    @Override
    public void kill() {
        // TODO Auto-generated method stub
        System.out.println("Stopping empty routine");

    }

}
